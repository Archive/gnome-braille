#include <stdio.h>
#include "../gnome-braille/braille-table-encoder.h"

int
main (int argc, char **argv)
{
    BrailleTable *table;
    BrailleTableEncoder *table_encoder;
    GIOChannel *outio;
    const gchar *outstring, *instring = "Braille Encoding Test: Input file \'test-braille-input.utf8\'.\n";
    guint *offsets = NULL, bytes;
    FILE *fp;
    gchar *line;
    gchar buff[1024];
    gchar *fname;

    g_type_init ();

    outio = g_io_channel_new_file ("test-log", "w", NULL);
    if (g_io_channel_set_encoding (outio, "UTF-8", NULL) != G_IO_STATUS_NORMAL)
	puts ("failed to set output encoding to UTF-8.");

    puts ("Read Table Test"); 

    if (argc > 2)
    fname = g_strconcat (g_getenv ("srcdir"), "/../tables/", argv[2], NULL);

    else
    fname = g_strconcat (g_getenv ("srcdir"), "/../tables/kana.tbl", NULL);

    g_message ("reading table... %s", fname);
    table = braille_table_construct_from_file (fname);
    g_free (fname);
    
    if (table == NULL) return 1;

    table_encoder = g_object_new (BRAILLE_TYPE_TABLE_ENCODER, NULL);
    braille_table_encoder_set_table (table_encoder, table);

    if (table_encoder == NULL) return 2;

    outstring = braille_encoder_translate_string (BRAILLE_ENCODER (table_encoder), instring, &offsets);

    puts (instring);
    puts (outstring);
    g_io_channel_write_chars (outio, outstring, -1, &bytes, NULL);

    if (argc > 1) 
	fp = fopen (argv[1], "r");
    else {
	fname = g_strconcat (g_getenv ("srcdir"), "/test-braille-input.utf8", NULL);
	fp = fopen (fname, "r");
	g_free (fname);
    }
    if (ferror (fp)) return 3;

    g_message ("reading input file...");

    while (fgets (buff, 1024, fp)) 
    {
	outstring = braille_encoder_translate_string (BRAILLE_ENCODER (table_encoder), buff, &offsets);
	g_io_channel_write_chars (outio, buff, -1, NULL, NULL);
	if (outstring && g_io_channel_write_chars (outio, outstring, -1, &bytes, NULL) != G_IO_STATUS_NORMAL)
	{
	    fprintf (stderr, "Error writing string %s\n", outstring);
	}
	else 
	{    
	    gint i, len = g_utf8_strlen (outstring, -1) - 1; /* omit terminal carriage-return */
	    for (i = 0; i < len; ++i)
	    {
		g_io_channel_write_unichar (outio, 0x0030 + offsets[i] % 10, NULL);
	    }
	    g_io_channel_write_unichar (outio, 0x000a, NULL);
	}
    }

    g_io_channel_shutdown (outio, TRUE, NULL);
    
    puts ("Success");

    return 0;
}
