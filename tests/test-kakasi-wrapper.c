#include <stdio.h>
#include <glib-object.h>


#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <libkakasi.h>

int main (int argc, char *argv[])
{
    GError *error = NULL;
    gsize bytes_read, bytes_written;
    gchar buf[1024];
    char *kbuf;

#ifdef HAVE_KAKASI
    int retval = kakasi_getopt_argv (argc, argv);

    while (fgets (buf, 1024, stdin))
    {
	gchar *eucout;
	gchar *utf8out;
	eucout = g_convert (buf, strlen (buf), "EUC-JP", "utf8", 
	       &bytes_read, &bytes_written, &error);
	if (error) 
	    g_message ("input conversion error: %s", error->message);
	else
	    g_message ("%d utf8 input bytes converted to %d kakasi input bytes", bytes_read, bytes_written);
	/* send 'eucout' to kakasi */
	kbuf = kakasi_do (eucout);
	g_free (eucout);

	utf8out = g_convert (kbuf, strlen (kbuf), "utf8", "EUC-JP",
			    &bytes_read, &bytes_written, &error);

	free (kbuf); /* ? */

	if (error) 
	    g_message ("output conversion error: %s", error->message);
	else
	    g_message ("%d kakasi output bytes converted to %d utf8 output bytes", bytes_read, bytes_written);

	fputs (utf8out, stdout);
	g_free (utf8out);
    }
    return retval;
#else
    return -1;
#endif
}
