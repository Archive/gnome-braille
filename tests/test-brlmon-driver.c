#include <stdio.h>
#include "../gnome-braille/drivers/braille-monitor-driver.h"

int
main (int argc, char **argv)
{
    BrailleDriver *driver;

    g_type_init ();

    driver = g_object_new (BRAILLE_TYPE_BRAILLE_MONITOR_DRIVER, NULL);

    braille_driver_display_string (driver, "Braille Monitor Driver test succeeded.", 0);

    braille_driver_display_string (driver, "\342\240\215\342\240\243\342\240\257", 0);

    sleep (1);
    braille_driver_scroll_display (driver, 0, 1, BRAILLE_DRIVER_SCROLL_CELLS);
    sleep (1);
    braille_driver_scroll_display (driver, 0, -1, BRAILLE_DRIVER_SCROLL_CELLS);
    sleep (1);
    braille_driver_scroll_display (driver, 0, -2, BRAILLE_DRIVER_SCROLL_CELLS);
    sleep (1);
    braille_driver_scroll_display (driver, 0, 2, BRAILLE_DRIVER_SCROLL_CELLS);
    sleep (1);
    braille_driver_scroll_display (driver, 0, -1, BRAILLE_DRIVER_SCROLL_PAGE);
    sleep (1);
    braille_driver_scroll_display (driver, 0, 1, BRAILLE_DRIVER_SCROLL_PAGE);

    g_main_loop_run (g_main_loop_new (NULL, TRUE));

    g_object_unref (driver);

    return 0;
}
