#include <dirent.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <arpa/nameser.h>
#include <resolv.h>
#include <netdb.h>
#include <string.h>
#include <glib-object.h>

#undef INET6

#if defined(AF_INET6) && defined(RES_USE_INET6)
#define RESOLV_SET_IPV6     _res.options |= RES_USE_INET6
#define RESOLV_UNSET_IPV6   _res.options &= ~RES_USE_INET6
#else
#define RESOLV_SET_IPV6
#define RESOLV_UNSET_IPV6
#endif

#define REMOTE_SERVER_PORT 7000

#ifdef HAVE_SOCKADDR_SA_LEN
#define SET_SOCKADDR_LEN(saddr, len)                     \
		((struct sockaddr *)(saddr))->sa_len = (len)
#else 
#define SET_SOCKADDR_LEN(saddr, len)
#endif

#ifdef INET6
    struct sockaddr_in6	cliAddr, 
			*remoteServAddr;
    #define FL_AF_INET	AF_INET6
    #define IP_TYPE	"IPv6"
    #define sockaddrin	sockaddr_in6
    #define SERV_HOST_ADDR	"::1"	/* host addr for server*/
#else
    struct sockaddr_in	cliAddr, 
			*remoteServAddr;
    #define FL_AF_INET	AF_INET
    #define IP_TYPE	"IPv"
    #define sockaddrin	sockaddr_in
    #define SERV_HOST_ADDR	"127.0.0.1"	/* host addr for server*/
#endif
#define MAX_MSG 100
#define BUFFER_SIZE 256

int sock = 0;
socklen_t     remoteServAddr_len;

/*
 * True if succeeded in mapping, else false.
 */
static int
ipv4_addr_from_addr (struct in_addr *dest_addr,
		     unsigned char  *src_addr,
		     int             src_length)
{
	if (src_length == 4)
		memcpy (dest_addr, src_addr, 4);

	else if (src_length == 16) {
		int i;

#ifdef LOCAL_DEBUG
		g_warning ("Doing conversion ...");
#endif

		/* An ipv6 address, might be an IPv4 mapped though */
		for (i = 0; i < 10; i++)
			if (src_addr [i] != 0)
				return 0;

		if (src_addr [10] != 0xff ||
		    src_addr [11] != 0xff)
			return 0;

		memcpy (dest_addr, &src_addr[12], 4);
	} else
		return 0;

	return 1;
}

struct sockaddr *
get_sockaddr (const char *hostname, const char *portnum, socklen_t *saddr_len)
{
    int foo;
#ifdef INET6
    struct sockaddr_in6 *saddr = g_new0 (struct sockaddr_in6, 1);
    struct hostent      *host = gethostbyname (hostname);
    *saddr_len = sizeof (struct sockaddr_in6);

    saddr->sin6_family = AF_INET6;
    saddr->sin_port = htons (atoi (portnum));
    memcpy (&saddr->sin6_addr, host->h_addr_list[0], sizeof (struct in6_addr));

    g_warning ("IPv6 ? incomplete impl");

    return (struct sockaddr *) saddr;
#else
    struct sockaddr_in  *saddr = g_new0 (struct sockaddr_in, 1);
    struct hostent      *host;
    *saddr_len = sizeof (struct sockaddr_in);

    SET_SOCKADDR_LEN (saddr, sizeof (struct sockaddr_in));
    saddr->sin_family = AF_INET;
    saddr->sin_port = htons (atoi (portnum));
 
    if ((saddr->sin_addr.s_addr = inet_addr (hostname)) == INADDR_NONE) {
	int i;
	
	RESOLV_UNSET_IPV6;
	if (!(_res.options & RES_INIT))
	    res_init();
	
	host = gethostbyname (hostname);
	if (!host) {
	    g_warning ("No host for %s", hostname);
	    g_free (saddr);
	    return NULL;
	}
	
	for(i = 0; host->h_addr_list[i]; i++)
	    if(ipv4_addr_from_addr (&saddr->sin_addr,
				    (unsigned char *)host->h_addr_list [i],
				    host->h_length))
		break;
	
	if(!host->h_addr_list[i]) {
	    g_free (saddr);
	    return NULL;
	}
    }

    return (struct sockaddr *) saddr;
#endif
}

int 
create_udp_client ()
{
    int sd = 0;

    if ((sd = socket(FL_AF_INET, SOCK_DGRAM, 0)) < 0) {
	printf("Can not open socket \n");
	exit(1);
	}

    /* bind local server port */
#ifdef INET6
    fprintf(stderr,"IPV6:%s",SERV_HOST_ADDR);
    cliAddr.sin6_family 	= FL_AF_INET;
    cliAddr.sin6_addr 		= in6addr_any;
    cliAddr.sin6_port 		= htons(0);
#else
    cliAddr.sin_family 		= FL_AF_INET;
    cliAddr.sin_addr.s_addr 	= htonl(INADDR_ANY);
    cliAddr.sin_port 		= htons(0);
#endif
  
    if (bind (sd, (struct sockaddr *) &cliAddr, sizeof(cliAddr)) < 0) {
	    exit(1);
	}

    remoteServAddr = (struct sockaddrin *) get_sockaddr (SERV_HOST_ADDR, "7000", &remoteServAddr_len);
    if (remoteServAddr == NULL) g_error ("Bad remoteServAddr");

    printf ("ServerUDP:Waiting for data on port UDP %u\n",REMOTE_SERVER_PORT);    return sd;
}

static int errno;

int
main (int argc, char *argv[])
{
    char buff[1024];
    char *msg;
    int len, result;
    sock = create_udp_client ();
    memset (buff, 0x0, 1024);
    buff[0] = 0x02;
    strcpy (&buff[1], "<BRLOUT clear=\"true\"><BRLDISP ID=\"0\" offset=\"0\"><TEXT>hello</TEXT></BRLDISP></BRLOUT>");
    len = strlen (buff);
    buff[len] = 0x03;
    buff[len+1] = 0x0;
    printf ("buffer contains %d bytes: %s\n", strlen(buff), buff);
    result = sendto (sock, buff, strlen (buff), 0, (struct sockaddr *) remoteServAddr,
	    remoteServAddr_len);
    msg = strerror (errno);
    if (result < 1 && errno) printf ("result=%d: [%d] %s", result, errno, msg); 
}
