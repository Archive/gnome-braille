#include <stdio.h>
#include <libbraille-driver.h>
#include <braille-table-encoder.h>

static BrailleTableEncoder *table_encoder;
static void braille_event_callback(BrailleEvent event);

static void
test_func (BrailleDriver *driver)
{
    gchar *instring;
    gchar *outstring;
    guint *offsets = NULL;

    instring = g_strdup ("Libbraille Driver test succeeded.");

    g_message ("displaying strings");
    braille_driver_display_string (driver, instring, 0);
    outstring = braille_encoder_translate_string (BRAILLE_ENCODER (table_encoder), instring, &offsets);
    braille_driver_display_string (driver, outstring, 1);
    g_free (instring);
    g_free (outstring);
    sleep (1);

    braille_driver_scroll_display (driver, 0, 1, BRAILLE_DRIVER_SCROLL_CELLS);
    braille_driver_scroll_display (driver, 1, 1, BRAILLE_DRIVER_SCROLL_CELLS);
    sleep (1);

    braille_driver_scroll_display (driver, 0, -1, BRAILLE_DRIVER_SCROLL_CELLS);
    braille_driver_scroll_display (driver, 1, -1, BRAILLE_DRIVER_SCROLL_CELLS);
    sleep (1);

    braille_driver_scroll_display (driver, 0, -2, BRAILLE_DRIVER_SCROLL_CELLS);
    braille_driver_scroll_display (driver, 1, -2, BRAILLE_DRIVER_SCROLL_CELLS);
    sleep (1);

    braille_driver_scroll_display (driver, 0, 2, BRAILLE_DRIVER_SCROLL_CELLS);
    braille_driver_scroll_display (driver, 1, 2, BRAILLE_DRIVER_SCROLL_CELLS);
    sleep (1);

    braille_driver_scroll_display (driver, 0, -1, BRAILLE_DRIVER_SCROLL_PAGE);
    braille_driver_scroll_display (driver, 1, -1, BRAILLE_DRIVER_SCROLL_PAGE);
    sleep (1);

    braille_driver_scroll_display (driver, 0, 1, BRAILLE_DRIVER_SCROLL_PAGE);
    braille_driver_scroll_display (driver, 1, 1, BRAILLE_DRIVER_SCROLL_PAGE);
    g_message ("done");
}

static BrailleDriver*
init_driver (void)
{
    gchar *instring;
    const gchar *outstring;
    BrailleTable *table;
    BrailleDriver *driver;
    gchar *fname;
    const gchar *srcdir;

    srcdir = g_getenv ("srcdir") ? g_getenv ("srcdir") : ".";
    fname = g_strconcat (srcdir, "/../tables/standard.tbl", NULL);

    table = braille_table_construct_from_file (fname);
    g_free (fname);
    if (table == NULL) return NULL;

    table_encoder = g_object_new (BRAILLE_TYPE_TABLE_ENCODER, NULL);
    braille_table_encoder_set_table (table_encoder, table);

    g_message ("creating driver");
    driver = g_object_new (BRAILLE_TYPE_LIBBRAILLE_DRIVER, NULL);
    g_message ("done");

	braille_event_source_set_event_handler (BRAILLE_EVENT_SOURCE(driver), braille_event_callback);

    return driver;
}

int
main (int argc, char **argv)
{
    BrailleDriver *driver;

    putenv ("GTK_MODULES=");

    g_type_init ();

    if (!(driver = init_driver ()))
	return 1;

    test_func (driver);

    g_object_unref (driver);
    g_object_unref (table_encoder);

    return 0;
}

static void
braille_event_callback (BrailleEvent event)
{
	if (event.type == BRAILLE_EXCEPTION_DRIVER)
	{
		g_warning ("Error in driver");
	}
	else if (event.type == BRAILLE_EVENT_KEY)
	{
		switch (event.key.keytype)
		{
		case BRAILLE_KEY_TYPE_DOTS:
			g_warning ("Dots key: %d", event.key.keycode);
			break;
		case BRAILLE_KEY_TYPE_CELL:
			g_warning ("Cursor key: %d", event.key.keycode);
			break;
		default:
			g_warning ("Unknown type of key: %d", event.key.keytype);
		}
	}
}
