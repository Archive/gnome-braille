#include <stdio.h>
#include <glib-object.h>
#include "../gnome-braille/braille-table.h"
#include "../gnome-braille/braille-table-encoder.h"
#include "../gnome-braille/encoders/kakasi-encoder.h"

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

int main (int argc, char *argv[])
{
    BrailleEncoder *kakasi_encoder, *kana_encoder = NULL;
    gchar buf[1024];
    gboolean dots = FALSE;

    g_type_init ();

    if ((argc > 1) && !strcmp (argv[1], "-dots")) 
	dots = TRUE;

    kakasi_encoder = g_object_new (BRAILLE_TYPE_KAKASI_ENCODER, NULL);

    while (fgets (buf, 1024, stdin))
    {
	guint *offsets;
	gchar *outbuf = braille_encoder_translate_string (kakasi_encoder, buf, &offsets);
	if (dots)
	{
	    gchar *dotsout;
	    if (!kana_encoder)
	    {
		BrailleTable *kana_table;
		kana_encoder = g_object_new (BRAILLE_TYPE_TABLE_ENCODER, NULL);
		kana_table = braille_table_construct_from_file ("kana.tbl");
		braille_table_encoder_set_table (
		    BRAILLE_TABLE_ENCODER (kana_encoder), kana_table);
	    }
	    dotsout = braille_encoder_translate_string (kana_encoder, outbuf, &offsets);
	    g_free (outbuf);
	    fputs (dotsout, stdout);
	}
	else     
	    fputs (outbuf, stdout);
    }

    g_object_unref (kakasi_encoder);

    return 0;
}
