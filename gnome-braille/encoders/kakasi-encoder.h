/**
 * gnome-braille, a braille encoding and output system for GNOME, 
 *                and Unix-like operating environments.
 *
 * Copyright Sun Microsystems Inc. 2004, 2005
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 **/

#ifndef __KAKASI_ENCODER_H__
#define __KAKASI_ENCODER_H__


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <glib-object.h>
#include "braille-encoder.h"
#include "braille-table.h"

#define BRAILLE_TYPE_KAKASI_ENCODER               (kakasi_encoder_get_type ())
#define BRAILLE_IS_KAKASI_ENCODER(obj)            G_TYPE_CHECK_INSTANCE_TYPE ((obj), BRAILLE_TYPE_KAKASI_ENCODER)
#define KAKASI_ENCODER(obj)               G_TYPE_CHECK_INSTANCE_CAST ((obj), BRAILLE_TYPE_KAKASI_ENCODER, KakasiEncoder)
#define KAKASI_ENCODER_GET_CLASS(obj)     G_TYPE_INSTANCE_GET_CLASS ((obj), BRAILLE_TYPE_KAKASI_ENCODER, KakasiEncoderClass)

typedef struct _KakasiEncoder KakasiEncoder;
typedef struct _KakasiEncoderClass KakasiEncoderClass;

/* 
 * base class which implements BrailleEncoder interface using 
 * the Kakasi translation system to convert Japanese UTF-8 into
 * kana or romaji.
 *
 * KakasiEncoder is a GObject Class derived from GObject.
 */
struct _KakasiEncoder
{
    GObject parent;

    gboolean is_romaji;

    BrailleEncoder *alternate;
};

struct _KakasiEncoderClass
{
    GObjectClass parent;

    void     (* set_romaji_mode) (KakasiEncoder *encoder, gboolean romaji);

    void (* reserved1) (void);
    void (* reserved2) (void);
};

void kakasi_encoder_set_romaji_mode (KakasiEncoder *encoder, gboolean is_romaji);

KakasiEncoder* kakasi_encoder_new (void);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __KAKASI_ENCODER_H__ */
