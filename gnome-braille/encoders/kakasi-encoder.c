/**
 * gnome-braille, a braille encoding and output system for GNOME, 
 *                and Unix-like operating environments.
 *
 * Copyright Sun Microsystems Inc. 2005
 *
 * Copyright Sun Microsystems Inc. 2005
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 **/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "kakasi-encoder.h"
#ifdef HAVE_KAKASI
#include <libkakasi.h>
#endif

/* BrailleTableEncoder implementations */
static void kakasi_encoder_real_set_romaji_mode (KakasiEncoder *encoder, gboolean romaji_mode);

/* BrailleEncoder implementations */
static gchar *kakasi_encoder_translate_string (BrailleEncoder *encoder, const gchar *string, guint **offsets);
static void kakasi_encoder_set_alternate  (BrailleEncoder *encoder, BrailleEncoder *alternate);

static void
braille_encoder_interface_init (BrailleEncoderInterface *iface)
{
    g_return_if_fail (iface != NULL);

    iface->translate_string = kakasi_encoder_translate_string;
    iface->set_alternate = kakasi_encoder_set_alternate;
}

static void
kakasi_encoder_init (KakasiEncoder *encoder)
{
    encoder->is_romaji = FALSE;
    encoder->alternate = NULL;
}

static void
kakasi_encoder_class_init (KakasiEncoderClass *klass)
{
    static int argc = 4;
    static char *argv[] = {"kakasi", "-Jk", "-ieuc", "-oeuc", NULL};
    klass->set_romaji_mode = kakasi_encoder_real_set_romaji_mode;
#ifdef HAVE_KAKASI
    kakasi_getopt_argv (argc, argv);
#endif
}

GType
kakasi_encoder_get_type (void)
{
    static GType type = 0;
    if (!type)
    {
	static const GTypeInfo info =
	    {
		sizeof (KakasiEncoderClass),
		(GBaseInitFunc) NULL,
		(GBaseFinalizeFunc) NULL,
		(GClassInitFunc) kakasi_encoder_class_init,
		(GClassFinalizeFunc) NULL,
		NULL,
		sizeof (KakasiEncoder),
		0,
		(GInstanceInitFunc) kakasi_encoder_init,
	    };

	static const GInterfaceInfo braille_encoder_info =
	    {
		(GInterfaceInitFunc) braille_encoder_interface_init,
		(GInterfaceFinalizeFunc) NULL,
		NULL
	    };

	type = g_type_register_static (G_TYPE_OBJECT, "KakasiEncoder", &info, 0);

	g_type_add_interface_static (type, BRAILLE_TYPE_ENCODER, &braille_encoder_info);

    }
    return type;
}

/* implementations of virtual funcs for BrailleTableEncoder */

static void
kakasi_encoder_set_alternate (BrailleEncoder *encoder, BrailleEncoder *alternate)
{
    g_return_if_fail (BRAILLE_IS_KAKASI_ENCODER (encoder));

    if (KAKASI_ENCODER (encoder)->alternate) 
	g_object_unref (KAKASI_ENCODER (encoder)->alternate);

    if (G_IS_OBJECT (alternate))
	g_object_ref (alternate);

    if (alternate != NULL)
	g_assert (BRAILLE_IS_ENCODER (alternate));

    KAKASI_ENCODER (encoder)->alternate = alternate;
}

static gchar*
kakasi_encoder_translate_string (BrailleEncoder *encoder, const gchar *string, guint **offsets)
{
    gchar *encoded;
    KakasiEncoder *kakasi_encoder;
    GError *error = NULL;
    gsize bytes_read, bytes_written;
    gchar *euc_in, *euc_out;
    gchar buf[1024];

    g_return_if_fail (BRAILLE_IS_KAKASI_ENCODER (encoder));

    kakasi_encoder = KAKASI_ENCODER (encoder);

    if (!string || !g_utf8_validate (string, -1, NULL)) 
    {
	g_warning ("Braille Encoder passed invalid UTF-8.");
	return NULL;
    }

#ifdef HAVE_KAKASI
    euc_in = g_convert (string, strlen (string), "EUC-JP", "utf8", 
			&bytes_read, &bytes_written, &error);
    if (error) 
    {
	g_message ("input conversion error: %s", error->message);
	g_free (error);
	encoded = NULL;
    }
    else    
    {
	euc_out = kakasi_do (euc_in);
	g_free (euc_in);
	encoded = g_convert (euc_out, strlen (euc_out), "utf8", "EUC-JP",
			     &bytes_read, &bytes_written, &error);
    }
    if (error)
    {
	g_message ("output conversion error: %s", error->message);
	g_free (error);
        encoded = NULL;
    }	
    else
    {
	free (euc_out);
    }
#else
    encoded = g_strdup ("Sorry, this version of gnome-braille was built without Kakasi support.");
#endif
    if (encoded && !g_utf8_validate (encoded, -1, NULL)) 
    {
	g_warning ("error encoding braille: invalid UTF-8 generated");
	return NULL;
    }
    return encoded;
}

void
kakasi_encoder_set_romaji_mode (KakasiEncoder *encoder, gboolean romaji_mode)
{
    KakasiEncoderClass *klass;
    g_return_if_fail (BRAILLE_IS_KAKASI_ENCODER (encoder));
    klass = KAKASI_ENCODER_GET_CLASS (encoder);
    if (klass->set_romaji_mode)
	(klass->set_romaji_mode) (encoder, romaji_mode);
}

void
kakasi_encoder_real_set_romaji_mode (KakasiEncoder *encoder, gboolean romaji_mode)
{
    KAKASI_ENCODER (encoder)->is_romaji = romaji_mode;
}
