/**
 * gnome-braille, a braille encoding and output system for GNOME, 
 *                and Unix-like operating environments.
 *
 * Copyright Sun Microsystems Inc. 2004
 *
 * Copyright Sun Microsystems Inc. 2004
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 **/

#include "braille-table-encoder.h"

typedef enum {
    TABLE_LINE_FORMAT_UNICODE,
    TABLE_LINE_FORMAT_UTF8
} TableLineFormat;

typedef struct {
    const gchar *string;
    BrailleContext *context;
} MatchStringKey;

typedef struct {
    const gchar *string;
    guint match_len;
} MatchStringValue;

extern BrailleContext* BRAILLE_CONTEXT_ALL;
extern BrailleContext* BRAILLE_CONTEXT_ALPHA;

/* BrailleTableEncoder implementations */
static void braille_table_encoder_real_set_table (BrailleTableEncoder *encoder, BrailleTable *table);

/* BrailleEncoder implementations */
static gchar *braille_table_encoder_translate_string (BrailleEncoder *encoder, const gchar *string, guint **offsets);
static void braille_table_encoder_set_alternate  (BrailleEncoder *encoder, BrailleEncoder *alternate);
void braille_table_encoder_set_table (BrailleTableEncoder *encoder, BrailleTable *table);

static void
braille_encoder_interface_init (BrailleEncoderInterface *iface)
{
    g_return_if_fail (iface != NULL);

    iface->translate_string = braille_table_encoder_translate_string;
    iface->set_alternate = braille_table_encoder_set_alternate;
}

static void
braille_table_encoder_init (BrailleTableEncoder *encoder)
{
    encoder->table = NULL;
    encoder->alternate = NULL;
}

static void
braille_table_encoder_class_init (BrailleTableEncoderClass *klass)
{
    klass->set_table = braille_table_encoder_real_set_table;
}

GType
braille_table_encoder_get_type (void)
{
    static GType type = 0;
    if (!type)
    {
	static const GTypeInfo info =
	    {
		sizeof (BrailleTableEncoderClass),
		(GBaseInitFunc) NULL,
		(GBaseFinalizeFunc) NULL,
		(GClassInitFunc) braille_table_encoder_class_init,
		(GClassFinalizeFunc) NULL,
		NULL,
		sizeof (BrailleTableEncoder),
		0,
		(GInstanceInitFunc) braille_table_encoder_init,
	    };

	static const GInterfaceInfo braille_encoder_info =
	    {
		(GInterfaceInitFunc) braille_encoder_interface_init,
		(GInterfaceFinalizeFunc) NULL,
		NULL
	    };

	type = g_type_register_static (G_TYPE_OBJECT, "BrailleTableEncoder", &info, 0);

	g_type_add_interface_static (type, BRAILLE_TYPE_ENCODER, &braille_encoder_info);

    }
    return type;
}

/**
 * braille_table_encoder_new:
 * @table: the #BrailleTable which should serve as the base for this encoder.
 *
 * Creates a #BrailleTable instance which uses @table for its 
 * dot or character conversions.  The @braille_table_encoder
 * instance should be freed with #braille_table_encoder_release () 
 * when it is no longer needed.
 *
 * returns: a newly-created #BrailleTableEncoder instance.
 **/
BrailleTableEncoder*
braille_table_encoder_new (BrailleTable *table)
{
     BrailleTableEncoder *table_encoder;

     table_encoder = g_object_new (BRAILLE_TYPE_TABLE_ENCODER, NULL);
     if (table_encoder == NULL)
	  return NULL;
     braille_table_encoder_set_table (table_encoder, table);
     return table_encoder;
}

/**
 * braille_table_encoder_release:
 * @encoder: the #BrailleTableEncoder which is being released by its client.
 *
 * Release a #BrailleTableEncoder from use.  Any associated resources
 * may be freed as a result.  Clients should not perform any operations on
 * a #BrailleTableEncoder after they have released it.
 **/
void
braille_table_encoder_release (BrailleTableEncoder *encoder)
{
    if (G_IS_OBJECT (encoder))
	g_object_unref (encoder);
}

/**
 * braille_table_encoder_set_table:
 * @encoder: the #BrailleTableEncoder instance on which to operate.
 * @table: the #BrailleTable which should serve as the base for this encoder.
 *
 * Sets the #BrailleTable instance which this encoder should use for its 
 * dot or character conversions, freeing any previous #BrailleTable which
 * this encoder was using.
 **/
void braille_table_encoder_set_table (BrailleTableEncoder *encoder, BrailleTable *table)
{
    BrailleTableEncoderClass *klass;
    g_return_if_fail (BRAILLE_IS_TABLE_ENCODER (encoder));
    klass = BRAILLE_TABLE_ENCODER_GET_CLASS (encoder);
    if (klass->set_table)
	(klass->set_table) (encoder, table);
}

/* implementations of virtual funcs for BrailleTableEncoder */
static void
braille_table_encoder_real_set_table (BrailleTableEncoder *encoder, BrailleTable *table)
{
    g_return_if_fail (BRAILLE_IS_TABLE_ENCODER (encoder));

    if (encoder->table) 
	g_free (encoder->table);
    encoder->table = table;
}

/* implementations of virtual funcs */
static void
braille_table_encoder_set_alternate (BrailleEncoder *encoder, BrailleEncoder *alternate)
{
    g_return_if_fail (BRAILLE_IS_TABLE_ENCODER (encoder));

    if (BRAILLE_TABLE_ENCODER (encoder)->alternate) 
	g_object_unref (BRAILLE_TABLE_ENCODER (encoder)->alternate);

    if (G_IS_OBJECT (alternate))
	g_object_ref (alternate);

    if (alternate != NULL)
	g_assert (BRAILLE_IS_ENCODER (alternate));

    BRAILLE_TABLE_ENCODER (encoder)->alternate = alternate;
}

static gchar*
braille_table_encoder_translate_string (BrailleEncoder *encoder, const gchar *string, guint **offsets)
{
    BrailleTableEncoder *table_encoder;
    gchar *encoded;

    g_return_if_fail (BRAILLE_IS_TABLE_ENCODER (encoder));

    table_encoder = BRAILLE_TABLE_ENCODER (encoder);

    if (table_encoder->table) {
	guint char_index = 0;
	gboolean chars_missing = FALSE;
	BrailleOutputBuffer *outbuf;
	BrailleContext *context = BRAILLE_CONTEXT_ALPHA; /* stores the context state */

	if (!g_utf8_validate (string, -1, NULL)) 
	{
	    g_warning ("Braille Encoder passed invalid UTF-8.");
	    return NULL;
	}

	outbuf = braille_output_buffer_new ();

	chars_missing = braille_table_encode_to_buffer (table_encoder->table, outbuf, &string, &char_index);
	
	/* the handoff below is technically chaining, not delegation.                        */
	/* we've turned it off since what we _really_ want is to chain to the next encoder->next */
	if (0 && chars_missing && braille_encoder_get_alternate (BRAILLE_ENCODER (table_encoder)) != NULL) 
	{
	    guint *delegate_offsets;
	    encoded = braille_encoder_translate_string (braille_encoder_get_alternate (BRAILLE_ENCODER (table_encoder)), 
							outbuf->out_string, 
							offsets != NULL ? &delegate_offsets : NULL);
	    g_free (outbuf->out_string);
	    if (offsets != NULL)
	    {
		*offsets = braille_encoder_chain_offsets (outbuf->offsets, outbuf->len, delegate_offsets, encoded ? g_utf8_strlen (encoded, -1) : 0);
	    }
	}
	else
	{
	    encoded = outbuf->out_string;
	    if (offsets) 
		*offsets = outbuf->offsets;
	}
	g_free (outbuf);

	if (encoded && !g_utf8_validate (encoded, -1, NULL)) 
	{
	    g_warning ("error encoding braille: invalid UTF-8 generated");
	    return NULL;
	}
	return encoded;
    }
    else
    {
	g_warning ("Attempting to encode braille without a valid braille table!\n");
	return g_strdup (string);
    }
}
