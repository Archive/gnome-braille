/*
 * gnome-braille, a braille encoding and output system for GNOME, 
 *                and Unix-like operating systems.
 *
 * Copyright Sun Microsystems Inc. 2004
 *
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __BRAILLE_DRIVER_H__
#define __BRAILLE_DRIVER_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <glib-object.h>

#define BRAILLE_TYPE_DRIVER               (braille_driver_get_type ())
#define BRAILLE_IS_DRIVER(obj)            G_TYPE_CHECK_INSTANCE_TYPE ((obj), BRAILLE_TYPE_DRIVER)
#define BRAILLE_DRIVER(obj)               G_TYPE_CHECK_INSTANCE_CAST ((obj), BRAILLE_TYPE_DRIVER, BrailleDriver)
#define BRAILLE_DRIVER_GET_INTERFACE(obj) G_TYPE_INSTANCE_GET_INTERFACE ((obj), BRAILLE_TYPE_DRIVER, BrailleDriverInterface)

typedef enum {
    BRAILLE_DRIVER_STATUS_NORMAL,  /* 'all systems go' for comm; i.e. 'success' */
    BRAILLE_DRIVER_STATUS_RETRY,   /* not ready, retry later */
    BRAILLE_DRIVER_STATUS_NOPERM,  /* access rejected to comm */
    BRAILLE_DRIVER_STATUS_NOIMPL,  /* the command is not implemented by the driver */
    BRAILLE_DRIVER_STATUS_BADPARAM,/* parameters were invalid */
    BRAILLE_DRIVER_STATUS_INVALID, /* the driver instance was invalid or non-working */
    BRAILLE_DRIVER_STATUS_IO_FAULT /* error in serial comm */
} BrailleDriverStatus;

typedef enum {
    BRAILLE_DRIVER_SCROLL_CELLS,
    BRAILLE_DRIVER_SCROLL_CHARS,
    BRAILLE_DRIVER_SCROLL_PAGE
} BrailleDriverScrollAction;

typedef struct _BrailleDriver BrailleDriver;
typedef struct _BrailleDriverInterface BrailleDriverInterface;

struct _BrailleDriverInterface
{
    GTypeInterface parent;

    BrailleDriverStatus (*display_string)        (BrailleDriver *driver, 
						  gchar *string, 
						  gint display);
 
    gint                (*switchpos_to_offset)   (BrailleDriver *driver, 
						  gint pos, 
						  gint display);

    void                (*set_unknown_char)      (BrailleDriver *driver, 
						  gunichar c);

    gint                (*get_num_displays)      (BrailleDriver *driver);

    gchar*              (*get_display_name)      (BrailleDriver *driver, 
						  gint display);

    gint                (*get_display_cellcount) (BrailleDriver *driver, 
						  gint display);

    BrailleDriverStatus (*scroll_display)        (BrailleDriver *driver, 
						  gint display_num, 
						  gint units_to_scroll, 
						  BrailleDriverScrollAction action);

    BrailleDriverStatus (*set_virtual_caret)     (BrailleDriver *driver,
						  gint caret_offset,
						  gint display);

    BrailleDriverStatus (*set_scrolling_switch)  (BrailleDriver *driver, 
						  gint display_num, 
						  gint switchnum, 
						  gint units_to_scroll, 
						  BrailleDriverScrollAction action,
						  gboolean notify);

};

GType braille_driver_get_type (void);

/**
 * braille_driver_display_string:
 * @driver: the #BrailleDriver object of interest.
 * @string: a UTF-8 string.
 * @display_num: a #gint indicating the target display are of the device.
 *
 * Output @string to braille-cell-area @display_num on the braille device.
 * 
 * @string is UTF-8 text, but is converted on output to
 * the character encoding appropriate to the device.
 * Unicode characters in @string which correspond to 
 * Braille code points (U+2800 to U+28FF) are converted to
 * braille-dot commands.
 **/
BrailleDriverStatus braille_driver_display_string (BrailleDriver *driver, 
						   gchar *string, 
						   gint display_num);

/**
 * braille_driver_switchpos_to_offset:
 * @driver: the #BrailleDriver object of interest.
 * @pos: the braille switch position being queried.
 * @display_num: a #gint indicating the target display are of the device.
 *
 * Converts a braille driver switch position to an offset in the currently
 * displayed character string.  This conversion will take into account any
 * scrolling operations currently in effect on the string.
 *
 * Return value: the character offset in the most-recently-displayed string
 * corresponding to switch @pos.
 **/
gint braille_driver_switchpos_to_offset (BrailleDriver *driver, gint pos, gint display_num);

/**
 * braille_driver_set_unknown_char:
 * @driver: the #BrailleDriver object of interest.
 * @c: a #gunichar in the range U+2800 to U+28FF.
 *
 * Specify the dot pattern (as a unicode character between
 * U+2800 and U+28FF) or character displayed on the display
 * when braille_driver_display_string is passed a character 
 * outside the range displayable by @driver.
 * This is only used when passing strings to the device, as
 * opposed to dot-pattern commands.
 **/
void braille_driver_set_unknown_char (BrailleDriver *driver, gunichar c);

/**
 * braille_driver_get_num_displays:
 * @driver: the #BrailleDriver object of interest.
 *
 * Return value: a #gint indicating the number of display regions in the driver.
 **/
gint braille_driver_get_num_displays (BrailleDriver *driver);

/**
 * braille_driver_get_display_name:
 * @driver: the #BrailleDriver object of interest.
 * @display_num: a #gint indicating the display region to query.
 *
 * Get a string identifying a display region of a #BrailleDriver.
 *
 * Return value: a UTF-8 string naming the display region, e.g. "status".
 **/
gchar *braille_driver_get_display_name (BrailleDriver *driver, gint display_num);

/**
 * braille_driver_get_display_cellcount:
 * @driver: the #BrailleDriver object of interest.
 * @display_num: a #gint indicating the display region to query.
 *
 * Get the number of cells in a given display region.
 *
 * Return value: a #gint giving the number of available cells in @display_num.
 **/
gint braille_driver_get_display_cellcount (BrailleDriver *driver, gint display_num);


/**
 * braille_driver_scroll_display:
 * @driver: the #BrailleDriver object of interest.
 * @display_num: a #gint indicating the display region to scroll.
 * @units_to_scroll: a #gint indicating the direction and magnitude of the
 *          scrolling operation.
 * @action: a #BrailleDriverScrollAction indicating the scrolling units, 
 *          e.g. cells, pages, characters.
 *
 * Scrolls a display region forwards or backwards.  If @action is
 * #BRAILLE_SCROLL_CELLS or #BRAILLE_SCROLL_CHARS, @units_to_scroll
 * indicates the number of cells or characters to scroll.  If @action
 * is #BRAILLE_SCROLL_PAGES, @units_to_scroll should be 1 or -1.  Negative
 * values indicate "backwards" scrolling.
 *
 * Return value: a #BrailleDriverStatus indicating whether the operation was
 *               successfully carried out.
 **/
BrailleDriverStatus braille_driver_scroll_display (BrailleDriver *driver, 
						   gint display_num, 
						   gint units_to_scroll, 
						   BrailleDriverScrollAction action);

/**
 * braille_driver_set_virtual_caret:
 * @driver: the #BrailleDriver object of interest.
 * @caret_offset: a #gint indicating the cell offset where the caret should be placed.
 * @display_num: a #gint indicating the display region to query.
 *
 * Place the display's virtual caret at cell @caret_offset.
 *
 * Return value: a #BrailleDriverStatus indicating whether the caret was 
 *               positioned as requested.
 **/
BrailleDriverStatus braille_driver_set_virtual_caret (BrailleDriver *driver,
						      gint caret_offset,
						      gint display);
/**
 * braille_driver_set_scrolling_switch:
 * @driver: the #BrailleDriver object of interest.
 * @display_num: a #gint indicating the display region to query.
 * @switchnum: a #gint corresponding to a switch number as reported by @driver.
 * @units_to_scroll: a #gint indicating the magnitude and direction of the action.
 * @action: a #BrailleDriverScrollAction type.
 *
 * Associate a particular switch on the braille device with a scrolling action, 
 * such that subsequent presses of this switch result in @display_num scrolling
 * @units_to_scroll units of the type indicated by @action.  For instance,
 * a call to
 *  status = braille_driver_set_scrolling_switch (driver, 0, 2, -1, BRAILLE_SCROLL_CELLS);
 * will mean, if status == BRAILLE_STATUS_NORMAL, that each subsequent press of switch 2
 * will cause display 0 to scroll one cell backwards.
 *
 * Return value: a #BrailleDriverStatus indicating whether the association was
 *               successfully carried made.
 **/
BrailleDriverStatus braille_driver_set_scrolling_switch (BrailleDriver *driver, 
							 gint display_num, 
							 gint switchnum, 
							 gint units_to_scroll, 
							 BrailleDriverScrollAction action,
							 gboolean notify);



#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __BRAILLE_DRIVER_H__ */
