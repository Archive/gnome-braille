/*
 * libbraille driver for gnome-braille
 *
 * Copyright Sébastien Sablé. 2005
 *
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <braille.h>

#include "libbraille-driver.h"

static BrailleDriverStatus libbraille_driver_display_string        (BrailleDriver *driver,
																	gchar *string,
																	gint display_num);
static gint                libbraille_driver_switchpos_to_offset   (BrailleDriver *driver,
																	gint pos,
																	gint display_num);
static void                libbraille_driver_set_unknown_char      (BrailleDriver *driver,
																	gunichar c);
static gint                libbraille_driver_get_num_displays      (BrailleDriver *driver);
static gchar*              libbraille_driver_get_display_name      (BrailleDriver *driver,
																	gint display_num);
static gint                libbraille_driver_get_display_cellcount (BrailleDriver *driver,
																	gint display_num);
static BrailleDriverStatus libbraille_driver_scroll_display        (BrailleDriver *driver,
																	gint display_num,
																	gint units_to_scroll,
																	BrailleDriverScrollAction action);
static BrailleDriverStatus libbraille_driver_set_virtual_caret     (BrailleDriver *driver,
																	gint caret_offset,
																	gint display);
static BrailleDriverStatus libbraille_driver_set_scrolling_switch  (BrailleDriver *driver,
																	gint display_num,
																	gint switchnum,
																	gint units_to_scroll,
																	BrailleDriverScrollAction action,
																	gboolean notify);
static void libbraille_driver_refresh_contents (LibbrailleDriver *driver, gint display_num);
void libbraille_driver_set_event_handler (BrailleEventSource *event_source, BrailleNotifyFunc callback);
static gpointer libbraille_driver_read_thread (gpointer data);


static void
braille_driver_interface_init (BrailleDriverInterface *iface)
{
    g_return_if_fail (iface != NULL);

    iface->display_string = libbraille_driver_display_string;
    iface->switchpos_to_offset = libbraille_driver_switchpos_to_offset;
    iface->set_unknown_char = libbraille_driver_set_unknown_char;
    iface->get_num_displays = libbraille_driver_get_num_displays;
    iface->get_display_name = libbraille_driver_get_display_name;
    iface->get_display_cellcount = libbraille_driver_get_display_cellcount;
    iface->scroll_display = libbraille_driver_scroll_display;
    iface->set_virtual_caret = libbraille_driver_set_virtual_caret;
    iface->set_scrolling_switch = libbraille_driver_set_scrolling_switch;
}

static void
braille_event_source_interface_init (BrailleEventSourceInterface *iface)
{
    g_return_if_fail (iface != NULL);

    iface->set_event_handler = libbraille_driver_set_event_handler;
}

static void
libbraille_driver_init (LibbrailleDriver *driver)
{
    braille_config (BRL_TABLE, "none");
    braille_config (BRL_DRIVER, "fake");
    
    if(!braille_init ())
    {
		g_warning ("error initializing libbraille driver: %s", braille_geterror ());
    }

    /* FIXME: the gnew_0 calls leak */
    driver->n_cells = braille_size ();
    driver->n_displays = 2;
    /*  driver->n_status_cells = braille_status_size(); */
    driver->scroll = g_new0 (gint, 2);
    /*    driver->caret = g_new0 (gint, 2); */
    driver->contents = g_new0 (gchar *, 2);
    driver->unknown = 0x2800;
	driver->event_callback = NULL;

	if (!g_thread_supported ()) g_thread_init (NULL);

	braille_timeout (-1);
	g_thread_create (libbraille_driver_read_thread, driver, FALSE, NULL);
}

static void
libbraille_driver_class_init (LibbrailleDriverClass *klass)
{
	klass->read_thread = libbraille_driver_read_thread;
    /* klass->set_host = libbraille_driver_set_host; */
}

GType
libbraille_driver_get_type (void)
{
    static GType type = 0;
    if (!type)
    {
	static const GTypeInfo info =
	    {
		sizeof (LibbrailleDriverClass),
		(GBaseInitFunc) NULL,
		(GBaseFinalizeFunc) NULL,
		(GClassInitFunc) libbraille_driver_class_init,
		(GClassFinalizeFunc) NULL,
		NULL,
		sizeof (LibbrailleDriver),
		0,
		(GInstanceInitFunc) libbraille_driver_init,
	    };

	static const GInterfaceInfo braille_driver_info =
	    {
		(GInterfaceInitFunc) braille_driver_interface_init,
		(GInterfaceFinalizeFunc) NULL,
		NULL
	    };

	static const GInterfaceInfo braille_event_source_info =
	    {
		(GInterfaceInitFunc) braille_event_source_interface_init,
		(GInterfaceFinalizeFunc) NULL,
		NULL
	    };

	type = g_type_register_static (G_TYPE_OBJECT, "LibbrailleDriver", &info, 0);

	g_type_add_interface_static (type, BRAILLE_TYPE_DRIVER, &braille_driver_info);

	g_type_add_interface_static (type, BRAILLE_TYPE_EVENT_SOURCE, &braille_event_source_info);

    }  
    return type;
}

/**
 * libbraille_driver_new:
 *
 * Creates a #LibbrailleDriver.  The @libbraille_driver
 * instance should be freed with #libbraille_driver_release () 
 * when it is no longer needed.
 *
 * returns: a newly-created #LibbrailleDriver instance.
 **/
LibbrailleDriver*
libbraille_driver_new (void)
{
    LibbrailleDriver *driver;

    driver = g_object_new (BRAILLE_TYPE_LIBBRAILLE_DRIVER, NULL);
    return driver;
}

/**
 * libbraille_driver_display_string:
 * @driver: the #BrailleDriver object of interest.
 * @string: a UTF-8 string.
 * @display_num: a #gint indicating the target display are of the device.
 *
 * Output @string to braille-cell-area @display_num on the braille device.
 * 
 * @string is UTF-8 text, but is converted on output to
 * the character encoding appropriate to the device.
 * Unicode characters in @string which correspond to 
 * Braille code points (U+2800 to U+28FF) are converted to
 * braille-dot commands.
 **/
BrailleDriverStatus
libbraille_driver_display_string (BrailleDriver *driver, gchar *string, 
								  gint display_num)
{
    BrailleDriverInterface *iface;

    g_return_val_if_fail (BRAILLE_IS_LIBBRAILLE_DRIVER (driver), BRAILLE_DRIVER_STATUS_INVALID);
    g_return_val_if_fail (display_num < LIBBRAILLE_DRIVER (driver)->n_displays
						  && display_num >= 0, 
						  BRAILLE_DRIVER_STATUS_INVALID);

    if (LIBBRAILLE_DRIVER (driver)->contents[display_num])
	g_free (LIBBRAILLE_DRIVER (driver)->contents[display_num]);

    LIBBRAILLE_DRIVER (driver)->contents[display_num] = g_strdup (string);

    libbraille_driver_refresh_contents (LIBBRAILLE_DRIVER (driver), display_num);

    return BRAILLE_DRIVER_STATUS_NORMAL;
}

/**
 * libbraille_driver_switchpos_to_offset:
 * @driver: the #BrailleDriver object of interest.
 * @pos: the braille switch position being queried.
 * @display_num: a #gint indicating the target display are of the device.
 *
 * Converts a braille driver switch position to an offset in the currently
 * displayed character string.  This conversion will take into account any
 * scrolling operations currently in effect on the string.
 *
 * Return value: the character offset in the most-recently-displayed string
 * corresponding to switch @pos, or "-1" on error.
 **/
gint
libbraille_driver_switchpos_to_offset (BrailleDriver *driver, gint pos,
									   gint display_num)
{
    BrailleDriverInterface *iface;
    g_return_val_if_fail (BRAILLE_IS_LIBBRAILLE_DRIVER (driver) &&
						  display_num < LIBBRAILLE_DRIVER (driver)->n_displays, 
						  -1);

    return pos + LIBBRAILLE_DRIVER (driver)->scroll[display_num - 1];
}

/**
 * libbraille_driver_set_unknown_char:
 * @driver: the #BrailleDriver object of interest.
 * @c: a #gunichar in the range U+2800 to U+28FF.
 *
 * Specify the dot pattern (as a unicode character between
 * U+2800 and U+28FF) or character displayed on the display
 * when braille_driver_display_string is passed a character 
 * outside the range displayable by @driver.
 * This is only used when passing strings to the device, as
 * opposed to dot-pattern commands.
 **/
void
libbraille_driver_set_unknown_char (BrailleDriver *driver, gunichar c)
{
    BrailleDriverInterface *iface;
    g_return_if_fail (BRAILLE_IS_LIBBRAILLE_DRIVER (driver));
    LIBBRAILLE_DRIVER (driver)->unknown = c;
}

/**
 * libbraille_driver_get_num_displays:
 * @driver: the #BrailleDriver object of interest.
 *
 * Return value: a #gint indicating the number of display regions in the driver.
 **/
gint
libbraille_driver_get_num_displays (BrailleDriver *driver)
{
    BrailleDriverInterface *iface;
    g_return_val_if_fail (BRAILLE_IS_LIBBRAILLE_DRIVER (driver), 0);
    return 2;
}

/**
 * libbraille_driver_get_display_name:
 * @driver: the #BrailleDriver object of interest.
 * @display_num: a #gint indicating the display region to query.
 *
 * Get a string identifying a display region of a #BrailleDriver.
 *
 * Return value: a UTF-8 string naming the display region, e.g. "status", 
 *      or NULL if no name is available.
 **/
gchar *
libbraille_driver_get_display_name (BrailleDriver *driver, gint display_num)
{
    BrailleDriverInterface *iface;
    g_return_val_if_fail (BRAILLE_IS_LIBBRAILLE_DRIVER (driver), NULL);
    return (display_num == 1 ? "status" : "primary");
}

/**
 * libbraille_driver_get_display_cellcount:
 * @driver: the #BrailleDriver object of interest.
 * @display_num: a #gint indicating the display region to query.
 *
 * Get the number of cells in a given display region.
 *
 * Return value: a #gint giving the number of available cells in @display_num, or -1 on error.
 **/
gint
libbraille_driver_get_display_cellcount (BrailleDriver *driver, gint display_num)
{
    BrailleDriverInterface *iface;
    g_return_val_if_fail (BRAILLE_IS_LIBBRAILLE_DRIVER (driver), -1);
    return (display_num == 0 ? LIBBRAILLE_DRIVER(driver)->n_cells : braille_statussize ());
}

/**
 * braille_driver_scroll_display:
 * @driver: the #BrailleDriver object of interest.
 * @display_num: a #gint indicating the display region to scroll.
 * @units_to_scroll: a #gint indicating the direction and magnitude of the
 *          scrolling operation.
 * @action: a #BrailleDriverScrollAction indicating the scrolling units, 
 *          e.g. cells, pages, characters.
 *
 * Scrolls a display region forwards or backwards.  If @action is
 * #BRAILLE_SCROLL_CELLS or #BRAILLE_SCROLL_CHARS, @units_to_scroll
 * indicates the number of cells or characters to scroll.  If @action
 * is #BRAILLE_SCROLL_PAGES, @units_to_scroll should be 1 or -1.  Negative
 * values indicate "backwards" scrolling.
 *
 * Return value: a #BrailleDriverStatus indicating whether the operation was
 *               successfully carried out.
 **/
BrailleDriverStatus libbraille_driver_scroll_display (BrailleDriver *driver, 
													  gint display_num, 
													  gint units_to_scroll, 
													  BrailleDriverScrollAction action)
{
    BrailleDriverInterface *iface;
    gint delta;
    g_return_val_if_fail (BRAILLE_IS_LIBBRAILLE_DRIVER (driver), BRAILLE_DRIVER_STATUS_INVALID);
    g_return_val_if_fail (display_num >= 0 && 
						  display_num < LIBBRAILLE_DRIVER (driver)->n_displays,
						  BRAILLE_DRIVER_STATUS_INVALID);

    switch (action)
    {
    case BRAILLE_DRIVER_SCROLL_PAGE:
	delta = units_to_scroll * braille_driver_get_display_cellcount (driver, display_num);
	break;
    case BRAILLE_DRIVER_SCROLL_CHARS:
    case BRAILLE_DRIVER_SCROLL_CELLS:
	/* these are the same for brlmon, but not all drivers. */
    default:
	delta = units_to_scroll;
    }
    LIBBRAILLE_DRIVER (driver)->scroll[display_num] += delta;
    libbraille_driver_refresh_contents (LIBBRAILLE_DRIVER (driver), display_num);

    return BRAILLE_DRIVER_STATUS_NORMAL;
}

static BrailleDriverStatus 
libbraille_driver_set_virtual_caret (BrailleDriver *driver, 
									 gint caret_offset, 
									 gint display)
{
    if (display > 1 || display < 0 || !driver || caret_offset < -1) 
		return BRAILLE_DRIVER_STATUS_INVALID;
    LIBBRAILLE_DRIVER (driver)->caret[display] = caret_offset;
    libbraille_driver_refresh_contents (LIBBRAILLE_DRIVER (driver), display);
    return BRAILLE_DRIVER_STATUS_NORMAL;
}

/**
 * libbraille_driver_set_scrolling_switch:
 * @driver: the #BrailleDriver object of interest.
 * @display_num: a #gint indicating the display region to query.
 * @switch: a #gint corresponding to a switch number as reported by @driver.
 * @units_to_scroll: a #gint indicating the magnitude and direction of the action.
 * @action: a #BrailleDriverScrollAction type.
 * @notify: whether to continue notifying clients of events on this switch.
 *
 * Associate a particular switch on the braille device with a scrolling action, 
 * such that subsequent presses of this switch result in @display_num scrolling
 * @units_to_scroll units of the type indicated by @action.  For instance,
 * a call to
 *  status = braille_driver_set_scrolling_switch (driver, 0, 2, -1, BRAILLE_SCROLL_CELLS);
 * will mean, if status == BRAILLE_STATUS_NORMAL, that each subsequent press of switch 2
 * will cause display 0 to scroll one cell backwards.
 *
 * Return value: a #BrailleDriverStatus indicating whether the association was
 *               successfully carried made.
 **/
BrailleDriverStatus
libbraille_driver_set_scrolling_switch (BrailleDriver *driver, 
										gint display_num, 
										gint switchnum, 
										gint units_to_scroll, 
										BrailleDriverScrollAction action,
										gboolean notify)
{
    BrailleDriverInterface *iface;
    g_return_val_if_fail (BRAILLE_IS_LIBBRAILLE_DRIVER (driver), BRAILLE_DRIVER_STATUS_INVALID);
    return BRAILLE_DRIVER_STATUS_NOIMPL;
}

/**
 * libbraille_driver_set_event_handler:
 * @event_source: the #BrailleEventSource to be listened to.
 * @callback: the #BrailleNotifyFunc which should be notified of events.
 *
 * Registers a callback function to be notified of braille events such
 * as braille display button presses and proximity switch events.
 **/
void
libbraille_driver_set_event_handler (BrailleEventSource *event_source,
									 BrailleNotifyFunc callback)
{
    g_return_if_fail (BRAILLE_IS_LIBBRAILLE_DRIVER (event_source));
    /* TODO: we leak the old one and discard, so we don't currently support multiple listeners here */
    LIBBRAILLE_DRIVER (event_source)->event_callback = callback;
}

static void
libbraille_driver_refresh_contents (LibbrailleDriver *driver,
									gint display_num)
{
    gchar *string;
    gchar buff[1024];
    gint scrollval = driver->scroll[display_num];

    /* indicate the virtual caret */

    /* scroll as necessary */
    if (scrollval < g_utf8_strlen (driver->contents[display_num], -1))
    {
		if (scrollval >= 0)
		{
			string = g_utf8_offset_to_pointer (driver->contents[display_num], scrollval);
		}
		else
		{
			gint i;
#ifdef DISPLAY_BRAILLE_EMPTY_DOTS
			for (i = 0; i < (scrollval * -3); i += 3)
			{
				strncpy (&buff[i], "\342\240\200", 3);
			}
#else /* display them as spaces instead */
			for (i = 0; i < (-scrollval); i++)
				buff[i] = ' ';
#endif
			strncpy (&buff[i], driver->contents[display_num], 1024-i);
			buff[1023] = '\0';
			string = buff;
		}
    }
    else
		string = " ";

    if(display_num == 0)
		braille_set_lcd (string, -1);
    else if(display_num == 1)
		braille_set_cells (string, -1);
    braille_render ();
}

static gpointer
libbraille_driver_read_thread (gpointer data)
{
	LibbrailleDriver *driver;
	brl_key key;
	BrailleEvent event;
	signed char status;

	driver = (LibbrailleDriver *) data;

	while (1)
    {
		status = braille_read (&key);
		if (status < 0)
		{
			g_warning ("error reading data from libbraille driver: %s", braille_geterror ());
			if (driver->event_callback)
			{
				event.type = BRAILLE_EXCEPTION_DRIVER;
				driver->event_callback (event);
			}
		}
		else if (status && driver->event_callback)
		{
			switch (key.type)
			{
			case BRL_NONE:
				break;
			case BRL_KEY:
				event.type = BRAILLE_EVENT_KEY;
				event.key.keytype = BRAILLE_KEY_TYPE_DOTS;
				event.key.keycode = key.braille;
				driver->event_callback (event);
				break;
			case BRL_CURSOR:
				event.type = BRAILLE_EVENT_KEY;
				event.key.keytype = BRAILLE_KEY_TYPE_CELL;
				event.key.keycode = key.code;
				driver->event_callback (event);
				break;
			default:
				break;
			}
		}
    }
	 
  return NULL;
}
