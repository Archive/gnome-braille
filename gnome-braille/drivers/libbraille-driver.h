/*
 * libbraille driver for gnome-braille
 *
 * Copyright Sébastien Sablé. 2005
 *
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __LIBBRAILLE_DRIVER_H__
#define __LIBBRAILLE_DRIVER_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <glib-object.h>
#include <braille-driver.h>
#include <braille-event-source.h>
#include <braille-table-encoder.h>

#define BRAILLE_TYPE_LIBBRAILLE_DRIVER               (libbraille_driver_get_type ())
#define BRAILLE_IS_LIBBRAILLE_DRIVER(obj)            G_TYPE_CHECK_INSTANCE_TYPE ((obj), BRAILLE_TYPE_LIBBRAILLE_DRIVER)
#define LIBBRAILLE_DRIVER(obj)               G_TYPE_CHECK_INSTANCE_CAST ((obj), BRAILLE_TYPE_LIBBRAILLE_DRIVER, LibbrailleDriver)
#define LIBBRAILLE_DRIVER_GET_CLASS(obj)     G_TYPE_INSTANCE_GET_CLASS ((obj), BRAILLE_TYPE_LIBBRAILLE_DRIVER, LibbrailleDriverClass)

typedef struct _LibbrailleDriver LibbrailleDriver;
typedef struct _LibbrailleDriverClass LibbrailleDriverClass;

struct _LibbrailleDriver
{
    GObject parent;

    /* private data, don't poke it */
    gint  n_cells;
    gint  n_displays;
    gint  n_status_cells;
    gunichar unknown;
	GThread* gthread;
    BrailleNotifyFunc event_callback;

    gchar **contents;
    gint   *scroll;
    gint   *caret;
};

struct _LibbrailleDriverClass
{
    GObjectClass parent;

	gpointer (*read_thread) (gpointer data);
    /*    void (*set_host) (LibbrailleDriver *driver, gchar *hostname_with_port); */
};

/**
 * braille_table_encoder_new:
 * @table: the #BrailleTable which should serve as the base for this encoder.
 *
 * Creates a #BrailleTable instance which uses @table for its 
 * dot or character conversions.  The #LibbrailleDriver
 * instance should be freed with #braille_driver_release () 
 * when it is no longer needed.
 *
 * returns: a newly-created #LibbrailleDriver instance.
 **/
LibbrailleDriver* libbraille_driver_new (void);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __LIBBRAILLE_DRIVER_H__ */
