/*
 * gnome-braille, a braille encoding and output system for GNOME, 
 *                and Unix-like operating systems.
 *
 * Copyright Sun Microsystems Inc. 2004
 *
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include <dirent.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <arpa/nameser.h>
#include <resolv.h>
#include <netdb.h>

#include "braille-monitor-driver.h"

#ifdef INET6
    struct sockaddr_in6	cliAddr, 
			*remoteServAddr;
    #define FL_AF_INET	AF_INET6
    #define IP_TYPE	"IPv6"
    #define sockaddrin	sockaddr_in6
    #define SERV_HOST_ADDR	"::1"	/* host addr for server*/
#else
    struct sockaddr_in	cliAddr, 
			*remoteServAddr;
    #define FL_AF_INET	AF_INET
    #define IP_TYPE	"IPv"
    #define sockaddrin	sockaddr_in
    #define SERV_HOST_ADDR	"127.0.0.1"	/* host addr for server*/
#endif

#ifdef HAVE_SOCKADDR_SA_LEN
#define SET_SOCKADDR_LEN(saddr, len)                     \
		((struct sockaddr *)(saddr))->sa_len = (len)
#else 
#define SET_SOCKADDR_LEN(saddr, len)
#endif

/**
 * This is a driver for the gnopernicus "brlmonitor" braille monitor,
 * an onscreen virtual braille device for testing and demonstrations.
 **/

static BrailleDriverStatus braille_monitor_driver_display_string        (BrailleDriver *driver, 
									 gchar *string, 
									 gint display_num);
static gint                braille_monitor_driver_switchpos_to_offset   (BrailleDriver *driver, 
									 gint pos, 
									 gint display_num);
static void                braille_monitor_driver_set_unknown_char      (BrailleDriver *driver, 
									 gunichar c);
static gint                braille_monitor_driver_get_num_displays      (BrailleDriver *driver);
static gchar*              braille_monitor_driver_get_display_name      (BrailleDriver *driver, 
									 gint display_num);
static gint                braille_monitor_driver_get_display_cellcount (BrailleDriver *driver, 
									 gint display_num);
static BrailleDriverStatus braille_monitor_driver_scroll_display        (BrailleDriver *driver, 
									 gint display_num, 
									 gint units_to_scroll, 
									 BrailleDriverScrollAction action);
static BrailleDriverStatus braille_monitor_driver_set_virtual_caret     (BrailleDriver *driver, 
									 gint caret_offset, 
									 gint display);
static BrailleDriverStatus braille_monitor_driver_set_scrolling_switch  (BrailleDriver *driver, 
									 gint display_num, 
									 gint switchnum, 
									 gint units_to_scroll, 
									 BrailleDriverScrollAction action,
									 gboolean notify);
void                braille_monitor_driver_set_host              (BrailleMonitorDriver *driver,
								  char *hostname_with_port);

void                braille_monitor_driver_set_event_handler     (BrailleEventSource *event_source,
								  BrailleNotifyFunc notify_func);

/* internal use */
static gchar*       braille_monitor_compose_xml                  (BrailleMonitorDriver *driver,
								  const gchar *string, 
								  gint display_num);

static void         braille_monitor_send_xml                     (BrailleMonitorDriver *driver,
								  const gchar *xmlstring);

static gint         braille_monitor_open_connect                 (BrailleMonitorDriver *driver);
static void         braille_monitor_close_connect                (BrailleMonitorDriver *driver);
static void         braille_monitor_driver_refresh_contents      (BrailleMonitorDriver *driver, 
								  gint display_num);

static void
braille_driver_interface_init (BrailleDriverInterface *iface)
{
    g_return_if_fail (iface != NULL);

    iface->display_string = braille_monitor_driver_display_string;
    iface->switchpos_to_offset = braille_monitor_driver_switchpos_to_offset;
    iface->set_unknown_char = braille_monitor_driver_set_unknown_char;
    iface->get_num_displays = braille_monitor_driver_get_num_displays;
    iface->get_display_name = braille_monitor_driver_get_display_name;
    iface->get_display_cellcount = braille_monitor_driver_get_display_cellcount;
    iface->scroll_display = braille_monitor_driver_scroll_display;
    iface->set_virtual_caret = braille_monitor_driver_set_virtual_caret;
    iface->set_scrolling_switch = braille_monitor_driver_set_scrolling_switch;
}

static void
braille_event_source_interface_init (BrailleEventSourceInterface *iface)
{
    g_return_if_fail (iface != NULL);

    iface->set_event_handler = braille_monitor_driver_set_event_handler;
}

static void
braille_monitor_driver_init (BrailleMonitorDriver *driver)
{
    /* FIXME: the gnew_0 calls leak */
    driver->port = 7000;
    driver->hostname = g_strdup ("127.0.0.1");
    driver->n_cells = 80;
    driver->n_displays = 2;
    driver->n_status_cells = 4;
    driver->scroll = g_new0 (gint, 2);
    driver->caret = g_new0 (gint, 2);
    driver->contents = g_new0 (gchar *, 2);
    driver->unknown = 0x2800;
    driver->sock = braille_monitor_open_connect (driver);
    if (driver->sock == 0) g_warning ("error initializing braille monitor driver");
}

static void
braille_monitor_driver_class_init (BrailleMonitorDriverClass *klass)
{
    klass->set_host = braille_monitor_driver_set_host;
}

GType
braille_monitor_driver_get_type (void)
{
    static GType type = 0;
    if (!type)
    {
	static const GTypeInfo info =
	    {
		sizeof (BrailleMonitorDriverClass),
		(GBaseInitFunc) NULL,
		(GBaseFinalizeFunc) NULL,
		(GClassInitFunc) braille_monitor_driver_class_init,
		(GClassFinalizeFunc) NULL,
		NULL,
		sizeof (BrailleMonitorDriver),
		0,
		(GInstanceInitFunc) braille_monitor_driver_init,
	    };

	static const GInterfaceInfo braille_driver_info =
	    {
		(GInterfaceInitFunc) braille_driver_interface_init,
		(GInterfaceFinalizeFunc) NULL,
		NULL
	    };

	static const GInterfaceInfo braille_event_source_info =
	    {
		(GInterfaceInitFunc) braille_event_source_interface_init,
		(GInterfaceFinalizeFunc) NULL,
		NULL
	    };

	type = g_type_register_static (G_TYPE_OBJECT, "BrailleDriverMonitor", &info, 0);

	g_type_add_interface_static (type, BRAILLE_TYPE_DRIVER, &braille_driver_info);

	g_type_add_interface_static (type, BRAILLE_TYPE_EVENT_SOURCE, &braille_event_source_info);

    }  
    return type;
}

/**
 * braille_monitor_driver_display_string:
 * @driver: the #BrailleDriver object of interest.
 * @string: a UTF-8 string.
 * @display_num: a #gint indicating the target display are of the device.
 *
 * Output @string to braille-cell-area @display_num on the braille device.
 * 
 * @string is UTF-8 text, but is converted on output to
 * the character encoding appropriate to the device.
 * Unicode characters in @string which correspond to 
 * Braille code points (U+2800 to U+28FF) are converted to
 * braille-dot commands.
 **/
BrailleDriverStatus braille_monitor_driver_display_string (BrailleDriver *driver, 
							   gchar *string, 
							   gint display_num)
{
    BrailleDriverInterface *iface;
    gchar *xmlstring;

    g_return_val_if_fail (BRAILLE_IS_MONITOR_DRIVER (driver), BRAILLE_DRIVER_STATUS_INVALID);
    g_return_val_if_fail (display_num < BRAILLE_MONITOR_DRIVER (driver)->n_displays
			  && display_num >= 0, 
			  BRAILLE_DRIVER_STATUS_INVALID);

    if (BRAILLE_MONITOR_DRIVER (driver)->contents[display_num])
	g_free (BRAILLE_MONITOR_DRIVER (driver)->contents[display_num]);
    BRAILLE_MONITOR_DRIVER (driver)->contents[display_num] = g_strdup (string);
    braille_monitor_driver_refresh_contents (BRAILLE_MONITOR_DRIVER (driver), display_num);

    return BRAILLE_DRIVER_STATUS_NORMAL;
}

/**
 * braille_monitor_driver_switchpos_to_offset:
 * @driver: the #BrailleDriver object of interest.
 * @pos: the braille switch position being queried.
 * @display_num: a #gint indicating the target display are of the device.
 *
 * Converts a braille driver switch position to an offset in the currently
 * displayed character string.  This conversion will take into account any
 * scrolling operations currently in effect on the string.
 *
 * Return value: the character offset in the most-recently-displayed string
 * corresponding to switch @pos, or "-1" on error.
 **/
gint braille_monitor_driver_switchpos_to_offset (BrailleDriver *driver, gint pos, gint display_num)
{
    BrailleDriverInterface *iface;
    g_return_val_if_fail (BRAILLE_IS_MONITOR_DRIVER (driver) &&
			  display_num < BRAILLE_MONITOR_DRIVER (driver)->n_displays, 
			  -1);

    return pos + BRAILLE_MONITOR_DRIVER (driver)->scroll[display_num - 1];
}

/**
 * braille_monitor_driver_set_unknown_char:
 * @driver: the #BrailleDriver object of interest.
 * @c: a #gunichar in the range U+2800 to U+28FF.
 *
 * Specify the dot pattern (as a unicode character between
 * U+2800 and U+28FF) or character displayed on the display
 * when braille_driver_display_string is passed a character 
 * outside the range displayable by @driver.
 * This is only used when passing strings to the device, as
 * opposed to dot-pattern commands.
 **/
void braille_monitor_driver_set_unknown_char (BrailleDriver *driver, gunichar c)
{
    BrailleDriverInterface *iface;
    g_return_if_fail (BRAILLE_IS_MONITOR_DRIVER (driver));
    BRAILLE_MONITOR_DRIVER (driver)->unknown = c;
}

/**
 * braille_monitor_driver_get_num_displays:
 * @driver: the #BrailleDriver object of interest.
 *
 * Return value: a #gint indicating the number of display regions in the driver.
 **/
gint braille_monitor_driver_get_num_displays (BrailleDriver *driver)
{
    BrailleDriverInterface *iface;
    g_return_val_if_fail (BRAILLE_IS_MONITOR_DRIVER (driver), 0);
    return 2;
}

/**
 * braille_monitor_driver_get_display_name:
 * @driver: the #BrailleDriver object of interest.
 * @display_num: a #gint indicating the display region to query.
 *
 * Get a string identifying a display region of a #BrailleDriver.
 *
 * Return value: a UTF-8 string naming the display region, e.g. "status", 
 *      or NULL if no name is available.
 **/
gchar *braille_monitor_driver_get_display_name (BrailleDriver *driver, gint display_num)
{
    BrailleDriverInterface *iface;
    g_return_val_if_fail (BRAILLE_IS_MONITOR_DRIVER (driver), NULL);
    return (display_num == 1 ? "status" : "primary");
}

/**
 * braille_monitor_driver_get_display_cellcount:
 * @driver: the #BrailleDriver object of interest.
 * @display_num: a #gint indicating the display region to query.
 *
 * Get the number of cells in a given display region.
 *
 * Return value: a #gint giving the number of available cells in @display_num, or -1 on error.
 **/
gint braille_monitor_driver_get_display_cellcount (BrailleDriver *driver, gint display_num)
{
    BrailleDriverInterface *iface;
    g_return_val_if_fail (BRAILLE_IS_MONITOR_DRIVER (driver), -1);
    return (display_num == 0 ? 40 : 4);
}


/**
 * braille_driver_scroll_display:
 * @driver: the #BrailleDriver object of interest.
 * @display_num: a #gint indicating the display region to scroll.
 * @units_to_scroll: a #gint indicating the direction and magnitude of the
 *          scrolling operation.
 * @action: a #BrailleDriverScrollAction indicating the scrolling units, 
 *          e.g. cells, pages, characters.
 *
 * Scrolls a display region forwards or backwards.  If @action is
 * #BRAILLE_SCROLL_CELLS or #BRAILLE_SCROLL_CHARS, @units_to_scroll
 * indicates the number of cells or characters to scroll.  If @action
 * is #BRAILLE_SCROLL_PAGES, @units_to_scroll should be 1 or -1.  Negative
 * values indicate "backwards" scrolling.
 *
 * Return value: a #BrailleDriverStatus indicating whether the operation was
 *               successfully carried out.
 **/
BrailleDriverStatus braille_monitor_driver_scroll_display (BrailleDriver *driver, 
							   gint display_num, 
							   gint units_to_scroll, 
							   BrailleDriverScrollAction action)
{
    BrailleDriverInterface *iface;
    gint delta;
    g_return_val_if_fail (BRAILLE_IS_MONITOR_DRIVER (driver), BRAILLE_DRIVER_STATUS_INVALID);
    g_return_val_if_fail (display_num >= 0 && 
			  display_num < BRAILLE_MONITOR_DRIVER (driver)->n_displays,
			  BRAILLE_DRIVER_STATUS_INVALID);
    switch (action)
    {
	case BRAILLE_DRIVER_SCROLL_PAGE:
	    delta = units_to_scroll * 40; /* we arbitrarily page by 40 in brlmon */
	    break;
	case BRAILLE_DRIVER_SCROLL_CHARS:
	case BRAILLE_DRIVER_SCROLL_CELLS:
	    /* these are the same for brlmon, but not all drivers. */
	default:
	    delta = units_to_scroll;
    }
    BRAILLE_MONITOR_DRIVER (driver)->scroll[display_num] += delta;
    braille_monitor_driver_refresh_contents (BRAILLE_MONITOR_DRIVER (driver), display_num);

    return BRAILLE_DRIVER_STATUS_NORMAL;
}

static BrailleDriverStatus 
braille_monitor_driver_set_virtual_caret     (BrailleDriver *driver, 
					      gint caret_offset, 
					      gint display)
{
    if (display > 1 || display < 0 || !driver || caret_offset < -1) 
	return BRAILLE_DRIVER_STATUS_INVALID;
    BRAILLE_MONITOR_DRIVER (driver)->caret[display] = caret_offset;
    braille_monitor_driver_refresh_contents (BRAILLE_MONITOR_DRIVER (driver), display);
    return BRAILLE_DRIVER_STATUS_NORMAL;
}

/**
 * braille_monitor_driver_set_scrolling_switch:
 * @driver: the #BrailleDriver object of interest.
 * @display_num: a #gint indicating the display region to query.
 * @switch: a #gint corresponding to a switch number as reported by @driver.
 * @units_to_scroll: a #gint indicating the magnitude and direction of the action.
 * @action: a #BrailleDriverScrollAction type.
 * @notify: whether to continue notifying clients of events on this switch.
 *
 * Associate a particular switch on the braille device with a scrolling action, 
 * such that subsequent presses of this switch result in @display_num scrolling
 * @units_to_scroll units of the type indicated by @action.  For instance,
 * a call to
 *  status = braille_driver_set_scrolling_switch (driver, 0, 2, -1, BRAILLE_SCROLL_CELLS);
 * will mean, if status == BRAILLE_STATUS_NORMAL, that each subsequent press of switch 2
 * will cause display 0 to scroll one cell backwards.
 *
 * Return value: a #BrailleDriverStatus indicating whether the association was
 *               successfully carried made.
 **/
BrailleDriverStatus braille_monitor_driver_set_scrolling_switch (BrailleDriver *driver, 
								 gint display_num, 
								 gint switchnum, 
								 gint units_to_scroll, 
								 BrailleDriverScrollAction action,
								 gboolean notify)
{
    BrailleDriverInterface *iface;
    g_return_val_if_fail (BRAILLE_IS_MONITOR_DRIVER (driver), BRAILLE_DRIVER_STATUS_INVALID);
    return BRAILLE_DRIVER_STATUS_NOIMPL;
}

/**
 * braille_monitor_driver_set_event_handler:
 * @event_source: the #BrailleEventSource to be listened to.
 * @botify_func: the #BrailleNotifyFunc which should be notified of events.
 *
 * Registers a callback function to be notified of braille events such
 * as braille display button presses and proximity switch events.
 **/
void                braille_monitor_driver_set_event_handler     (BrailleEventSource *event_source,
								  BrailleNotifyFunc notify_func)
{
    g_return_if_fail (BRAILLE_IS_MONITOR_DRIVER (event_source));
    /* TODO: we leak the old one and discard, so we don't currently support multiple listeners here */
    /* It doesn't much matter since brlmonitor doesn't support braille switch events yet */
    BRAILLE_MONITOR_DRIVER (event_source)->event_callback = notify_func;
}


void
braille_monitor_driver_set_host (BrailleMonitorDriver *driver, gchar *hostname_with_port)
{
    gchar **tokens;
    g_return_if_fail (BRAILLE_IS_MONITOR_DRIVER (driver));
    tokens = g_strsplit (hostname_with_port, ":", 2);
    BRAILLE_MONITOR_DRIVER (driver)->hostname = tokens[0];
    if (tokens[1]) BRAILLE_MONITOR_DRIVER (driver)->port = atoi(tokens[1]);
}

static 
gchar*       
braille_monitor_compose_xml                  (BrailleMonitorDriver *driver,
					      const gchar *string, 
					      gint display_num)
{
    gchar pos[4];
    snprintf (pos, 3, "%d", driver->caret[display_num] - driver->scroll[display_num]);
    return g_strconcat ("\002","<BRLOUT clear=\"true\"><BRLDISP ID=\"", (display_num == 0) ? "0" : "1", 
			"\" offset=\"0\" cursor=\"",
			pos,
			"\"><TEXT>", 
			string,
			"</TEXT></BRLDISP></BRLOUT>", "\003", NULL);
}

static void         
braille_monitor_send_xml                     (BrailleMonitorDriver *driver,
					      const gchar *xmlstring)
{
    gint result = sendto (driver->sock, xmlstring, strlen (xmlstring), 0, 
	    (struct sockaddr *) driver->serv_addr,
		driver->serv_addr_len);
    if (result < 0) 
	g_warning ("error sending xml string to braille monitor");
}

/*
 * True if succeeded in mapping, else false.
 */
static int
ipv4_addr_from_addr (struct in_addr *dest_addr,
		     unsigned char  *src_addr,
		     int             src_length)
{
	if (src_length == 4)
		memcpy (dest_addr, src_addr, 4);

	else if (src_length == 16) {
		int i;

#ifdef LOCAL_DEBUG
		g_warning ("Doing conversion ...");
#endif

		/* An ipv6 address, might be an IPv4 mapped though */
		for (i = 0; i < 10; i++)
			if (src_addr [i] != 0)
				return 0;

		if (src_addr [10] != 0xff ||
		    src_addr [11] != 0xff)
			return 0;

		memcpy (dest_addr, &src_addr[12], 4);
	} else
		return 0;

	return 1;
}

static gboolean
sock_in_cb (GIOChannel *source, GIOCondition condition, gpointer data)
{
    g_message ("incoming");
    return TRUE;
}

static gint         
braille_monitor_open_connect                 (BrailleMonitorDriver *driver)
{
    gint sock = 0, i;
    struct hostent *host;

    if ((sock = socket (FL_AF_INET, SOCK_DGRAM, 0)) < 0) 
	return 0;
    driver->client_addr = g_new0 (struct sockaddr_in, 1);
    if (bind (sock, (struct sockaddr *) &driver->client_addr, sizeof (*driver->client_addr)) < 0)
	return 0;
    driver->serv_addr = g_new0 (struct sockaddr_in, 1);
    driver->serv_addr->sin_family = AF_INET;
    driver->serv_addr->sin_port = htons (driver->port);
    SET_SOCKADDR_LEN (driver->serv_addr, sizeof (struct sockaddr_in));
    driver->serv_addr_len = sizeof (struct sockaddr_in);
    if ((driver->serv_addr->sin_addr.s_addr = inet_addr (driver->hostname)) == INADDR_NONE)
    {
	host = gethostbyname (driver->hostname);
	if (!host) return 0;
	for (i = 0; host->h_addr_list[i]; i++)
	{
	    if (ipv4_addr_from_addr (&driver->serv_addr->sin_addr, 
				     (unsigned char*) host->h_addr_list[i],
				     host->h_length))
		break;
	}
	if (!host->h_addr_list[i]) return 0;
    }
    
    g_io_add_watch (g_io_channel_unix_new (sock), G_IO_IN | G_IO_PRI, sock_in_cb, NULL);
    
    return sock;
}

static void         
braille_monitor_close_connect                (BrailleMonitorDriver *driver)
{
    ;
}

static void
braille_monitor_driver_refresh_contents             (BrailleMonitorDriver *driver,
						     gint display_num)
{
    gchar *string, *xmlstring;
    gchar buff[1024];
    gint scrollval = driver->scroll[display_num];

    /* indicate the virtual caret */
    

    /* scroll as necessary */
    if (scrollval < g_utf8_strlen (driver->contents[display_num], -1))
    {
	if (scrollval >= 0)
	{
	    string = 
		g_utf8_offset_to_pointer (driver->contents[display_num], scrollval);
	}
	else
	{
	    gint i;
#ifdef DISPLAY_BRAILLE_EMPTY_DOTS
	    for (i = 0; i < (scrollval * -3); i += 3)
	    {
		strncpy (&buff[i], "\342\240\200", 3);
	    }
#else /* display them as spaces instead */
	    for (i = 0; i < (-scrollval); i++)
		buff[i] = ' ';
#endif
	    strncpy (&buff[i], driver->contents[display_num], 1024-i);
	    buff[1023] = '\0';
	    string = buff;
	}
    }
    else
	string = " ";

    xmlstring = braille_monitor_compose_xml (BRAILLE_MONITOR_DRIVER (driver), 
					     string, display_num);
    braille_monitor_send_xml (BRAILLE_MONITOR_DRIVER (driver), xmlstring);
    g_free (xmlstring);
}
