/*
 * gnome-braille, a braille encoding and output system for GNOME, 
 *                and Unix-like operating systems.
 *
 * Copyright Sun Microsystems Inc. 2004
 *
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __BRLMON_DRIVER_H__
#define __BRLMON_DRIVER_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <glib-object.h>
#include <braille-driver.h>
#include <braille-event-source.h>

#define BRAILLE_TYPE_BRAILLE_MONITOR_DRIVER               (braille_monitor_driver_get_type ())
#define BRAILLE_IS_MONITOR_DRIVER(obj)            G_TYPE_CHECK_INSTANCE_TYPE ((obj), BRAILLE_TYPE_BRAILLE_MONITOR_DRIVER)
#define BRAILLE_MONITOR_DRIVER(obj)               G_TYPE_CHECK_INSTANCE_CAST ((obj), BRAILLE_TYPE_BRAILLE_MONITOR_DRIVER, BrailleMonitorDriver)
#define BRAILLE_MONITOR_DRIVER_GET_CLASS(obj)     G_TYPE_INSTANCE_GET_CLASS ((obj), BRAILLE_TYPE_BRAILLE_MONITOR_DRIVER, BrailleMonitorDriverClass)

typedef struct _BrailleMonitorDriver BrailleMonitorDriver;
typedef struct _BrailleMonitorDriverClass BrailleMonitorDriverClass;

struct _BrailleMonitorDriver
{
    GObject parent;

    /* private data, don't poke it */
    guint  port;
    gchar *hostname;
    gint  n_cells;
    gint  n_displays;
    gint  n_status_cells;
    gunichar unknown;
    BrailleNotifyFunc event_callback;

    gchar **contents;
    gint   *scroll;
    gint   *caret;

    struct sockaddr_in* client_addr;
    struct sockaddr_in* serv_addr;
    size_t  serv_addr_len;
    gint   sock;
};

struct _BrailleMonitorDriverClass
{
    GObjectClass parent;

    void (*set_host) (BrailleMonitorDriver *driver, gchar *hostname_with_port);
};

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __BRLMON_ENCODER_H__ */
