/*
 * BrlAPI driver for gnome-braille
 *
 * Copyright S�bastien Sabl�, Samuel Thibault. 2005
 *
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <brltty/api.h>
#include <brltty/brldefs.h>
#include <string.h>
#include <stdio.h>

#include "brlapi-driver.h"

static BrailleDriverStatus brlapi_driver_display_string        (BrailleDriver *driver,
																	gchar *string,
																	gint display_num);
static gint                brlapi_driver_switchpos_to_offset   (BrailleDriver *driver,
																	gint pos,
																	gint display_num);
static void                brlapi_driver_set_unknown_char      (BrailleDriver *driver,
																	gunichar c);
static gint                brlapi_driver_get_num_displays      (BrailleDriver *driver);
static gchar*              brlapi_driver_get_display_name      (BrailleDriver *driver,
																	gint display_num);
static gint                brlapi_driver_get_display_cellcount (BrailleDriver *driver,
																	gint display_num);
static BrailleDriverStatus brlapi_driver_scroll_display        (BrailleDriver *driver,
																	gint display_num,
																	gint units_to_scroll,
																	BrailleDriverScrollAction action);
static BrailleDriverStatus brlapi_driver_set_virtual_caret     (BrailleDriver *driver,
																	gint caret_offset,
																	gint display);
static BrailleDriverStatus brlapi_driver_set_scrolling_switch  (BrailleDriver *driver,
																	gint display_num,
																	gint switchnum,
																	gint units_to_scroll,
																	BrailleDriverScrollAction action,
																	gboolean notify);
static void brlapi_driver_refresh_contents (BrlapiDriver *driver, gint display_num);
void brlapi_driver_set_event_handler (BrailleEventSource *event_source, BrailleNotifyFunc callback);
static gpointer brlapi_driver_read_thread (gpointer data);


static void
braille_driver_interface_init (BrailleDriverInterface *iface)
{
    g_return_if_fail (iface != NULL);

    iface->display_string = brlapi_driver_display_string;
    iface->switchpos_to_offset = brlapi_driver_switchpos_to_offset;
    iface->set_unknown_char = brlapi_driver_set_unknown_char;
    iface->get_num_displays = brlapi_driver_get_num_displays;
    iface->get_display_name = brlapi_driver_get_display_name;
    iface->get_display_cellcount = brlapi_driver_get_display_cellcount;
    iface->scroll_display = brlapi_driver_scroll_display;
    iface->set_virtual_caret = brlapi_driver_set_virtual_caret;
    iface->set_scrolling_switch = brlapi_driver_set_scrolling_switch;
}

static void
braille_event_source_interface_init (BrailleEventSourceInterface *iface)
{
    g_return_if_fail (iface != NULL);

    iface->set_event_handler = brlapi_driver_set_event_handler;
}

static void
brlapi_driver_init (BrlapiDriver *driver)
{
    brlapi_settings_t settings;
    unsigned int x, y;
    
    if(brlapi_initializeConnection (NULL,&settings) < 0)
    {
	brlapi_perror("error initializing brlapi connection");
	return;
    }

    if (brlapi_getDisplaySize (&x, &y) < 0)
    {
	brlapi_perror("getting brlapi display size");
	return;
    }
    if (brlapi_getTty(-1, NULL) < 0) {
	brlapi_perror("getting brlapi tty");
	return;
    }
      
    /* FIXME: the gnew_0 calls leak */

    driver->n_cells = x * y;
    driver->n_displays = 1;
    /* TODO: status cells */
    /*  driver->n_status_cells = braille_status_size(); */
    driver->scroll = g_new0 (gint, 1);
    /*    driver->caret = g_new0 (gint, 1); */
    driver->contents = g_new0 (gchar *, 1);
    driver->unknown = 0x2800;
    driver->event_callback = NULL;
    
    if (!g_thread_supported ()) g_thread_init (NULL);
    
    g_thread_create (brlapi_driver_read_thread, driver, FALSE, NULL);
}

static void
brlapi_driver_class_init (BrlapiDriverClass *klass)
{
    klass->read_thread = brlapi_driver_read_thread;
    /* klass->set_host = brlapi_driver_set_host; */
}

GType
brlapi_driver_get_type (void)
{
    static GType type = 0;
    if (!type)
    {
	static const GTypeInfo info =
	    {
		sizeof (BrlapiDriverClass),
		(GBaseInitFunc) NULL,
		(GBaseFinalizeFunc) NULL,
		(GClassInitFunc) brlapi_driver_class_init,
		(GClassFinalizeFunc) NULL,
		NULL,
		sizeof (BrlapiDriver),
		0,
		(GInstanceInitFunc) brlapi_driver_init,
	    };

	static const GInterfaceInfo braille_driver_info =
	    {
		(GInterfaceInitFunc) braille_driver_interface_init,
		(GInterfaceFinalizeFunc) NULL,
		NULL
	    };

	static const GInterfaceInfo braille_event_source_info =
	    {
		(GInterfaceInitFunc) braille_event_source_interface_init,
		(GInterfaceFinalizeFunc) NULL,
		NULL
	    };

	type = g_type_register_static (G_TYPE_OBJECT, "BrlapiDriver", &info, 0);

	g_type_add_interface_static (type, BRAILLE_TYPE_DRIVER, &braille_driver_info);

	g_type_add_interface_static (type, BRAILLE_TYPE_EVENT_SOURCE, &braille_event_source_info);

    }  
    return type;
}

/**
 * brlapi_driver_new:
 *
 * Creates a #BrlapiDriver.  The @brlapi_driver
 * instance should be freed with #brlapi_driver_release () 
 * when it is no longer needed.
 *
 * returns: a newly-created #BrlapiDriver instance.
 **/
BrlapiDriver*
brlapi_driver_new (void)
{
    BrlapiDriver *driver;

    driver = g_object_new (BRAILLE_TYPE_BRLAPI_DRIVER, NULL);
    return driver;
}

/**
 * brlapi_driver_display_string:
 * @driver: the #BrailleDriver object of interest.
 * @string: a UTF-8 string.
 * @display_num: a #gint indicating the target display are of the device.
 *
 * Output @string to braille-cell-area @display_num on the braille device.
 * 
 * @string is UTF-8 text, but is converted on output to
 * the character encoding appropriate to the device.
 * Unicode characters in @string which correspond to 
 * Braille code points (U+2800 to U+28FF) are converted to
 * braille-dot commands.
 **/
BrailleDriverStatus brlapi_driver_display_string (BrailleDriver *driver,
							   gchar *string, 
							   gint display_num)
{
    BrailleDriverInterface *iface;

    g_return_val_if_fail (BRAILLE_IS_BRLAPI_DRIVER (driver), BRAILLE_DRIVER_STATUS_INVALID);
    g_return_val_if_fail (display_num < BRLAPI_DRIVER (driver)->n_displays
						  && display_num >= 0, 
						  BRAILLE_DRIVER_STATUS_INVALID);

    if (BRLAPI_DRIVER (driver)->contents[display_num])
	g_free (BRLAPI_DRIVER (driver)->contents[display_num]);

    BRLAPI_DRIVER (driver)->contents[display_num] = g_strdup (string);

    brlapi_driver_refresh_contents (BRLAPI_DRIVER (driver), display_num);

    return BRAILLE_DRIVER_STATUS_NORMAL;
}

/**
 * brlapi_driver_switchpos_to_offset:
 * @driver: the #BrailleDriver object of interest.
 * @pos: the braille switch position being queried.
 * @display_num: a #gint indicating the target display are of the device.
 *
 * Converts a braille driver switch position to an offset in the currently
 * displayed character string.  This conversion will take into account any
 * scrolling operations currently in effect on the string.
 *
 * Return value: the character offset in the most-recently-displayed string
 * corresponding to switch @pos, or "-1" on error.
 **/
gint
brlapi_driver_switchpos_to_offset (BrailleDriver *driver, gint pos,
									   gint display_num)
{
    BrailleDriverInterface *iface;
    g_return_val_if_fail (BRAILLE_IS_BRLAPI_DRIVER (driver) &&
			  display_num < BRLAPI_DRIVER (driver)->n_displays, 
			  -1);

    return pos + BRLAPI_DRIVER (driver)->scroll[display_num];
}

/**
 * brlapi_driver_set_unknown_char:
 * @driver: the #BrailleDriver object of interest.
 * @c: a #gunichar in the range U+2800 to U+28FF.
 *
 * Specify the dot pattern (as a unicode character between
 * U+2800 and U+28FF) or character displayed on the display
 * when braille_driver_display_string is passed a character 
 * outside the range displayable by @driver.
 * This is only used when passing strings to the device, as
 * opposed to dot-pattern commands.
 **/
void
brlapi_driver_set_unknown_char (BrailleDriver *driver, gunichar c)
{
    BrailleDriverInterface *iface;
    g_return_if_fail (BRAILLE_IS_BRLAPI_DRIVER (driver));
    BRLAPI_DRIVER (driver)->unknown = c;
}

/**
 * brlapi_driver_get_num_displays:
 * @driver: the #BrailleDriver object of interest.
 *
 * Return value: a #gint indicating the number of display regions in the driver.
 **/
gint
brlapi_driver_get_num_displays (BrailleDriver *driver)
{
    BrailleDriverInterface *iface;
    g_return_val_if_fail (BRAILLE_IS_BRLAPI_DRIVER (driver), 0);
    return BRLAPI_DRIVER (driver)->n_displays;
}

/**
 * brlapi_driver_get_display_name:
 * @driver: the #BrailleDriver object of interest.
 * @display_num: a #gint indicating the display region to query.
 *
 * Get a string identifying a display region of a #BrailleDriver.
 *
 * Return value: a UTF-8 string naming the display region, e.g. "status", 
 *      or NULL if no name is available.
 **/
gchar *
brlapi_driver_get_display_name (BrailleDriver *driver, gint display_num)
{
    BrailleDriverInterface *iface;
    g_return_val_if_fail (BRAILLE_IS_BRLAPI_DRIVER (driver), NULL);
    return (display_num == 1 ? "status" : "primary");
}

/**
 * brlapi_driver_get_display_cellcount:
 * @driver: the #BrailleDriver object of interest.
 * @display_num: a #gint indicating the display region to query.
 *
 * Get the number of cells in a given display region.
 *
 * Return value: a #gint giving the number of available cells in @display_num, or -1 on error.
 **/
gint
brlapi_driver_get_display_cellcount (BrailleDriver *driver, gint display_num)
{
    BrailleDriverInterface *iface;
    g_return_val_if_fail (BRAILLE_IS_BRLAPI_DRIVER (driver), -1);
	g_return_val_if_fail (display_num == 0, -1);
    return BRLAPI_DRIVER(driver)->n_cells;
}

/**
 * braille_driver_scroll_display:
 * @driver: the #BrailleDriver object of interest.
 * @display_num: a #gint indicating the display region to scroll.
 * @units_to_scroll: a #gint indicating the direction and magnitude of the
 *          scrolling operation.
 * @action: a #BrailleDriverScrollAction indicating the scrolling units, 
 *          e.g. cells, pages, characters.
 *
 * Scrolls a display region forwards or backwards.  If @action is
 * #BRAILLE_SCROLL_CELLS or #BRAILLE_SCROLL_CHARS, @units_to_scroll
 * indicates the number of cells or characters to scroll.  If @action
 * is #BRAILLE_SCROLL_PAGES, @units_to_scroll should be 1 or -1.  Negative
 * values indicate "backwards" scrolling.
 *
 * Return value: a #BrailleDriverStatus indicating whether the operation was
 *               successfully carried out.
 **/
BrailleDriverStatus brlapi_driver_scroll_display (BrailleDriver *driver, 
							   gint display_num, 
							   gint units_to_scroll, 
							   BrailleDriverScrollAction action)
{
    BrailleDriverInterface *iface;
    gint delta;
    fprintf(stderr,"scroll %d\n",units_to_scroll);
    g_return_val_if_fail (BRAILLE_IS_BRLAPI_DRIVER (driver), BRAILLE_DRIVER_STATUS_INVALID);
    g_return_val_if_fail (display_num >= 0 && 
			  display_num < BRLAPI_DRIVER (driver)->n_displays,
			  BRAILLE_DRIVER_STATUS_INVALID);

    switch (action)
    {
	case BRAILLE_DRIVER_SCROLL_PAGE:
	    delta = units_to_scroll * braille_driver_get_display_cellcount (driver, display_num);
	    break;
	case BRAILLE_DRIVER_SCROLL_CHARS:
	case BRAILLE_DRIVER_SCROLL_CELLS:
	    /* these are the same for brlmon, but not all drivers. */
	default:
	    delta = units_to_scroll;
    }
    BRLAPI_DRIVER (driver)->scroll[display_num] += delta;
    brlapi_driver_refresh_contents (BRLAPI_DRIVER (driver), display_num);

    return BRAILLE_DRIVER_STATUS_NORMAL;
}

static BrailleDriverStatus 
brlapi_driver_set_virtual_caret (BrailleDriver *driver, 
					 gint caret_offset, 
					 gint display)
{
    if (display > 1 || display < 0 || !driver || caret_offset < -1) 
	return BRAILLE_DRIVER_STATUS_INVALID;
    BRLAPI_DRIVER (driver)->caret[display] = caret_offset;
    brlapi_driver_refresh_contents (BRLAPI_DRIVER (driver), display);
    return BRAILLE_DRIVER_STATUS_NORMAL;
}

/**
 * brlapi_driver_set_scrolling_switch:
 * @driver: the #BrailleDriver object of interest.
 * @display_num: a #gint indicating the display region to query.
 * @switch: a #gint corresponding to a switch number as reported by @driver.
 * @units_to_scroll: a #gint indicating the magnitude and direction of the action.
 * @action: a #BrailleDriverScrollAction type.
 * @notify: whether to continue notifying clients of events on this switch.
 *
 * Associate a particular switch on the braille device with a scrolling action, 
 * such that subsequent presses of this switch result in @display_num scrolling
 * @units_to_scroll units of the type indicated by @action.  For instance,
 * a call to
 *  status = braille_driver_set_scrolling_switch (driver, 0, 2, -1, BRAILLE_SCROLL_CELLS);
 * will mean, if status == BRAILLE_STATUS_NORMAL, that each subsequent press of switch 2
 * will cause display 0 to scroll one cell backwards.
 *
 * Return value: a #BrailleDriverStatus indicating whether the association was
 *               successfully carried made.
 **/
BrailleDriverStatus brlapi_driver_set_scrolling_switch (BrailleDriver *driver, 
								gint display_num, 
								gint switchnum, 
								gint units_to_scroll, 
								BrailleDriverScrollAction action,
								gboolean notify)
{
    BrailleDriverInterface *iface;
    g_return_val_if_fail (BRAILLE_IS_BRLAPI_DRIVER (driver), BRAILLE_DRIVER_STATUS_INVALID);
    return BRAILLE_DRIVER_STATUS_NOIMPL;
}

/**
 * brlapi_driver_set_event_handler:
 * @event_source: the #BrailleEventSource to be listened to.
 * @callback: the #BrailleNotifyFunc which should be notified of events.
 *
 * Registers a callback function to be notified of braille events such
 * as braille display button presses and proximity switch events.
 **/
void
brlapi_driver_set_event_handler (BrailleEventSource *event_source,
									 BrailleNotifyFunc callback)
{
    g_return_if_fail (BRAILLE_IS_BRLAPI_DRIVER (event_source));
    /* TODO: we leak the old one and discard, so we don't currently support multiple listeners here */
    BRLAPI_DRIVER (event_source)->event_callback = callback;
}

static void
brlapi_driver_refresh_contents		(BrlapiDriver *driver,
						     gint display_num)
{
    brlapi_writeStruct ws = BRLAPI_WRITESTRUCT_INITIALIZER;
    gint scrollval = driver->scroll[display_num];
    gint n = driver->n_cells;
    gchar buff[n*6+1], *str, *str2;
    gint i=0,len,bytes;

/* indicate the virtual caret */
    if (scrollval < 0) {
#ifdef DISPLAY_BRAILLE_EMPTY_DOTS
	for (; n && scrollval++; i+=3, n--)
	    memcpy (&buff[i], "\342\240\200", 3);
#else /* display them as spaces instead */
	for (; n && scrollval++; i++, n--)
	    buff[i] = ' ';
#endif
    }
    len = g_utf8_strlen (driver->contents[display_num], -1);
    if (scrollval < len) {
	str = g_utf8_offset_to_pointer (driver->contents[display_num], scrollval);
        len -= scrollval;
	if (len > n)
	    len = n;
	bytes = g_utf8_offset_to_pointer (str, len) - str;
	memcpy (&buff[i], str, bytes);
	i += bytes;
	n -= len;
    }
#ifdef DISPLAY_BRAILLE_EMPTY_DOTS
    for (; n--; i += 3)
	memcpy (&buff[i], "\342\240\200", 3);
#else /* display them as spaces instead */
    for (; n--; i++)
	buff[i] = ' ';
#endif
    buff[i] = '\0';

    ws.text = buff;
    ws.charset = "UTF-8";
    if (brlapi_write(&ws) < 0)
	brlapi_perror("writing on brlapi display");
}

static gpointer
brlapi_driver_read_thread (gpointer data)
{
    BrlapiDriver *driver;
    brl_keycode_t key;
    BrailleEvent event;
    int status;

    driver = (BrlapiDriver *) data;

    while (1)
    {
	status = brlapi_readKey (1, &key);
	if (status < 0)
	{
	    brlapi_perror ("error reading data from brlapi driver");
	    if (driver->event_callback)
	    {
		event.type = BRAILLE_EXCEPTION_DRIVER;
		driver->event_callback (event);
	    }
	}
	else if (status && driver->event_callback)
	{
	    switch (key & BRL_MSK_BLK)
	    {
		case 0:
		    switch(key & BRL_MSK_ARG)
		    {
			case BRL_CMD_NOOP: break;

			case BRL_CMD_LNUP:
			case BRL_CMD_LNDN:
			case BRL_CMD_WINUP:
			case BRL_CMD_WINDN:
			case BRL_CMD_PRDIFLN:
			case BRL_CMD_NXDIFLN:
			case BRL_CMD_ATTRUP:
			case BRL_CMD_ATTRDN:
			case BRL_CMD_TOP:
			case BRL_CMD_BOT:
			case BRL_CMD_TOP_LEFT:
			case BRL_CMD_BOT_LEFT:
			case BRL_CMD_PRPGRPH:
			case BRL_CMD_NXPGRPH:
			case BRL_CMD_PRPROMPT:
			case BRL_CMD_NXPROMPT:
			case BRL_CMD_PRSEARCH:
			case BRL_CMD_NXSEARCH:
			    break;

			case BRL_CMD_CHRLT:
			case BRL_CMD_CHRRT:
			case BRL_CMD_HWINLT:
			case BRL_CMD_HWINRT:
			case BRL_CMD_FWINLT:
			case BRL_CMD_FWINRT:
			case BRL_CMD_FWINLTSKIP:
			case BRL_CMD_FWINRTSKIP:
			case BRL_CMD_LNBEG:
			case BRL_CMD_LNEND:
			    break;

			case BRL_CMD_HOME:
			case BRL_CMD_BACK:
			case BRL_CMD_RETURN:
			    break;

			case BRL_CMD_FREEZE:
			case BRL_CMD_PASTE:
			    break;
		    }
		    break;

		case BRL_BLK_CUTBEGIN:
		case BRL_BLK_CUTAPPEND:
		case BRL_BLK_CUTRECT:
		case BRL_BLK_CUTLINE:
		case BRL_BLK_PRINDENT:
		case BRL_BLK_NXINDENT:
		case BRL_BLK_DESCCHAR:
		case BRL_BLK_SETLEFT:
		case BRL_BLK_SETMARK:
		case BRL_BLK_GOTOMARK:
		case BRL_BLK_GOTOLINE:
		case BRL_BLK_PRDIFCHAR:
		case BRL_BLK_NXDIFCHAR:
		case BRL_BLK_PASSKEY:
		case BRL_BLK_PASSAT2:
		    break;
		case BRL_BLK_PASSDOTS:
		    event.type = BRAILLE_EVENT_KEY;
		    event.key.keytype = BRAILLE_KEY_TYPE_DOTS;
		    event.key.keycode = key & BRL_MSK_ARG;
		    driver->event_callback (event);
		    break;
		case BRL_BLK_PASSCHAR:
		    event.type = BRAILLE_EVENT_KEY;
		    event.key.keytype = BRAILLE_KEY_TYPE_UCS4;
		    event.key.keycode = key & BRL_MSK_ARG;
		    driver->event_callback (event);
		    break;
		case BRL_BLK_ROUTE:
		    event.type = BRAILLE_EVENT_KEY;
		    event.key.keytype = BRAILLE_KEY_TYPE_CELL;
		    event.key.keycode = key & BRL_MSK_ARG;
		    driver->event_callback (event);
		    break;
		default:
		    break;
	    }
	}
    }
	 
  return NULL;
}
