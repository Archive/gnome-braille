/**
 * gnome-braille, a braille encoding and output system for GNOME, 
 *                and Unix-like operating environments.
 *
 * Copyright Sun Microsystems Inc. 2004
 *
 * Copyright Sun Microsystems Inc. 2004
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 **/

#include "braille-table.h"

typedef enum {
    TABLE_LINE_FORMAT_UNICODE,
    TABLE_LINE_FORMAT_UTF8
} TableLineFormat;

typedef struct {
    const gchar *string;
    BrailleWordState match_type;
    BrailleContext *context;
} MatchStringKey;

typedef struct {
    const gchar *string;
    guint match_len;
} MatchStringValue;

/* BrailleTable implementations */
static gchar *braille_table_decompose_and_lookup_char (BrailleTable *table, gunichar ucs, BrailleContext **context);
static gchar *braille_table_lookup_char (BrailleTable *table, gunichar ucs, BrailleContext **context);

extern BrailleContext* BRAILLE_CONTEXT_ALL;
extern BrailleContext* BRAILLE_CONTEXT_ALPHA;

static gchar *
braille_table_decompose_and_lookup_char (BrailleTable *table, gunichar ucs, 
					 BrailleContext **context)
{
    unsigned int res_len;
    gunichar *chars = g_unicode_canonical_decomposition (ucs, &res_len);
    if (res_len > 1)
    {
	int i;
	gchar *result = NULL;
	for (i = 0; i < res_len; ++i)
	{
	    char cbuf[6];
	    char sbuf[201];
	    gchar *lookup = braille_table_lookup_char (table, 
						       chars[i], 
						       context);
	    if (result)
	    {
		gchar *tmp = result;
		result = g_strconcat (result, lookup, NULL);
	        g_free (tmp);
	    }
	    else
	    {
		result = g_strdup (lookup);
	    }
	}
	g_free (chars);
	return result;
    }
    g_free (chars);
    return NULL;
}

static gchar *
braille_table_lookup_char (BrailleTable *table, gunichar ucs, BrailleContext **context)
{
    GUnicodeBreakType type = g_unichar_break_type (ucs);
    gchar *decomposition = NULL;
    gint i = 0;

    if (type == G_UNICODE_BREAK_CARRIAGE_RETURN || type == G_UNICODE_BREAK_LINE_FEED) 
    {
	*context = BRAILLE_CONTEXT_ALPHA; /* should we always reset this way? */
	return g_strdup ("\n");
    }
    if (ucs >= 0x2800 && ucs <= 0x28ff) /* pass-through any prebrailled characters */
    {
	return g_ucs4_to_utf8 (&ucs, 1, NULL, NULL, NULL);
    }

    while (i < table->n_blocks)
    {
	UCSBlock *block = table->blocks[i];
	if (ucs >= block->min && ucs <= block->max && 
	    ((block->context == BRAILLE_CONTEXT_ALL) || 
	     braille_context_match (block->context, *context)))
	{
	    const gchar *cells;
	    if (block->out_context != BRAILLE_CONTEXT_ALL) 
	    {
		*context = block->out_context;
	    }
	    cells = block->data[ucs - block->min];
	    return cells ? g_strdup (cells) : NULL;
	}
	++i;
    }
    /* try decomposing the character and then looking it up */
    if (decomposition = braille_table_decompose_and_lookup_char (table, ucs, context))
    {
	/* FIXME: this introduces a memory management problem... we leak this string */
	return decomposition;
    }
    
    return NULL;
}

BrailleWordState 
match_string_type_from_string (gchar *string)
{
    if (!g_ascii_strcasecmp (string, "EXACT")) return BRAILLE_STRING_MATCH_EXACT;
    else if (!g_ascii_strcasecmp (string, "PREFIX")) return BRAILLE_STRING_MATCH_PREFIX;
    else if (!g_ascii_strcasecmp (string, "SUFFIX")) return BRAILLE_STRING_MATCH_SUFFIX;
    else return BRAILLE_STRING_MATCH_ALL;
}

gboolean
match_string_func (gpointer kp, gpointer vp, gpointer mp)
{
    MatchStringKey *key = kp;
    MatchStringKey *match = mp;

    g_assert (match);
    g_assert (match->context);
    g_assert (match->context->name);
    
    return (kp && vp && mp && g_str_has_prefix (match->string, key->string)
            && 
	    ((key->context == BRAILLE_CONTEXT_ALL) || !strcmp (key->context->name, match->context->name))
	    && 
	    ((key->match_type == BRAILLE_STRING_MATCH_ALL) || (key->match_type == match->match_type)));
}

static gchar*
braille_table_match_string (BrailleTable *table, BrailleContext *context, BrailleWordState match_type, const gchar **char_ptr, gint *char_index)
{
    /* TODO */
    MatchStringKey key = {*char_ptr, BRAILLE_STRING_MATCH_ALL, context};
    MatchStringValue *value = NULL;
    key.match_type = match_type;
    if (table->strings && strlen(*char_ptr) && (value = g_hash_table_find (table->strings, match_string_func, &key)))
    {
	if (char_index)
	    *char_index += value->match_len;

	* (char_ptr) = g_utf8_offset_to_pointer (*char_ptr, value->match_len);
	return g_strdup (value->string);
    }
    else
    {
	return NULL;
    }
}

static gchar*
braille_table_encode_chunk (BrailleTable *table, BrailleTable **last_used_table, 
			    BrailleContext **context, BrailleWordState *match_type,
			    const gchar **char_ptr, gint *char_index, gboolean *end_of_input)
{
    gchar *cells = NULL;
    const gchar *res_char_ptr = *char_ptr, *next_char_ptr;
    gchar *prefix = NULL;
    gunichar ucs = 0;
    BrailleTable *current_table = *last_used_table;
    gboolean in_main_table = (table == *last_used_table);

    if (*char_ptr) 
    {
	ucs = g_utf8_get_char (*char_ptr);
	next_char_ptr = g_utf8_next_char (*char_ptr);
    }
    while (current_table && !cells && res_char_ptr)
    {
	/* first we do substring/context matching, then try character lookup */
	res_char_ptr = *char_ptr;
	if (!(cells = braille_table_match_string (current_table, *context, *match_type, &res_char_ptr, char_index)))
	{
	    if (ucs)
	    {
		res_char_ptr = next_char_ptr;
		/* detect suffix and modify context if needed: usually a 1-char lookahead will do */
		if (prefix = braille_table_check_suffix (current_table, (const gchar *) res_char_ptr, context))
		{
		    if (!strlen (prefix))
			prefix = NULL; 
		}
		if (cells = braille_table_lookup_char (current_table, ucs, context))
		{
		    if (char_index)
			++(* char_index);
		}
		else 
		{
		    if (*char_ptr == next_char_ptr)
			res_char_ptr = NULL;
		    else
			res_char_ptr = next_char_ptr;
		}
	    }
	    else 
	    {
		res_char_ptr = NULL;
	    }
	}
	else
	{
	    next_char_ptr = res_char_ptr;
	}
	if (!cells) 
	{
	    if (in_main_table)
	    {
		if (current_table->alternate) 
		{
		    current_table = current_table->alternate;
		}
		else if (ucs) 
		{
		    if (char_index)
			++ (*char_index);
		    if (table->pass_through)
		    {
#ifdef UNKNOWN_CHAR_DEBUG
			cells = g_strdup_printf ("[U+%x]", ucs);
#else
			cells = g_new0 (gchar, 6);
			g_unichar_to_utf8 (ucs, cells);
#endif
		    }
		    else
		    {
			if (table->out_of_range)
			{
			    cells = g_strdup (table->out_of_range);
			}
			else
			{
			    cells = g_strdup ("\x28\x00");
			}
		    }
		}
	    }
	    else 
	    {
		current_table = table;
		in_main_table = TRUE;
	    }
	}
    }

    *char_ptr = res_char_ptr;

    *end_of_input = (!next_char_ptr || !g_utf8_get_char (next_char_ptr));

    *last_used_table = current_table;

    if (prefix) 
    {
	gchar *tmp = g_strconcat (prefix, cells, NULL);
	g_free (cells);
	cells = tmp;
    }
    return cells;
}

#define context_change_applies(c, o) \
(((c) != (o)->last_used_table) && !((o)->flags & BRAILLE_ENCODER_FLAGS_NO_CONTEXT_TAGS))

#define context_push_applies(t, c, o) \
(((t) != (c)) && (t)->context_push_string && strlen ((t)->context_push_string))

#define context_pop_applies(t, c, o) \
(((t) != (o)->last_used_table) && (t)->context_pop_string && strlen ((t)->context_pop_string))

static void
braille_table_append_to_buffer (BrailleOutputBuffer *outbuf, const gchar *cells, gint char_index)
{
    guint len;
    guint chunk_size = 1024;
    guint i = outbuf->len;

    if (cells && (len = strlen (cells)))
    {
	outbuf->bytelen += len;
	if (outbuf->bytelen > outbuf->size)
	{
	    outbuf->size += chunk_size;
	    outbuf->out_string = g_realloc (outbuf->out_string, outbuf->size);
	}
	len = g_utf8_strlen (cells, -1);
	outbuf->len += len;
	if (outbuf->len > outbuf->offset_size)
	{
	    outbuf->offset_size += chunk_size;
	    outbuf->offsets = g_realloc (outbuf->offsets, sizeof (guint) * outbuf->size);
	}
	g_strlcat (outbuf->out_string, cells, outbuf->size);
	for (; i < outbuf->len; ++i) 
	{
	    outbuf->offsets[i] = char_index;
	}
    }
}

static gboolean
braille_table_encode_chunk_to_buffer (BrailleTable *table, BrailleOutputBuffer *outbuf, const gchar **char_ptr, gint *char_index)
{
    static gint chunk_size = 1024;
    gchar *cells = NULL;
    BrailleTable *current_table = outbuf->last_used_table;
    gboolean end_of_input = FALSE;
    guint initial_char_index = 0;

    if (char_index) 
	initial_char_index = *char_index;

    g_assert (char_ptr != NULL);

    cells = braille_table_encode_chunk (table, &current_table, &outbuf->context,
					&outbuf->match_state,
					char_ptr, char_index, &end_of_input);

    if (cells)
    {
	if (context_change_applies (current_table, outbuf))
	{
	    if (context_pop_applies (table, current_table, outbuf))
	    {
		braille_table_append_to_buffer (outbuf, table->context_pop_string, initial_char_index ? 
		    initial_char_index - 1 : 0);
	    }
	    if (context_push_applies (table, current_table, outbuf))
	    {
		BrailleOutputBuffer *tmp_buf = braille_output_buffer_new ();
		const gchar *locale = (const gchar *) current_table->locales[0];
		gchar *push_context_string;

		tmp_buf->flags = BRAILLE_ENCODER_FLAGS_NO_CONTEXT_TAGS;
		
		braille_table_encode_to_buffer (table, tmp_buf, &locale, 
						NULL);
		push_context_string = g_strdup_printf (table->context_push_string, 
						       tmp_buf->out_string ?
						       tmp_buf->out_string : "");
		braille_output_buffer_free (tmp_buf);
		braille_table_append_to_buffer (outbuf, push_context_string, initial_char_index);
	    }
	}

	if (end_of_input && !(outbuf->flags & BRAILLE_ENCODER_FLAGS_NO_CONTEXT_TAGS) &&
	    context_pop_applies (table, current_table, outbuf)) /* end of buffer, pop contexts if needed */
	{
	    gchar *pop_context_string = table->context_pop_string;
	    if (!strcmp (cells, "\n"))
	    {
		if (pop_context_string)
		    braille_table_append_to_buffer (outbuf, pop_context_string, initial_char_index);
		braille_table_append_to_buffer (outbuf, cells, initial_char_index);
	    }
	    else
	    {
		braille_table_append_to_buffer (outbuf, cells, initial_char_index);
		if (pop_context_string)
		    braille_table_append_to_buffer (outbuf, pop_context_string, char_index ? *char_index : 0);
	    }
	}
	else
	{
	    braille_table_append_to_buffer (outbuf, cells, initial_char_index);
	}
	outbuf->last_used_table = current_table;

	g_free (cells);
	return TRUE;
    }
    return FALSE;
}

/* returns: whether any chars were missing from the conversion */
gboolean
braille_table_encode_to_buffer (BrailleTable *table, BrailleOutputBuffer *outbuf, const gchar **char_ptr, gint *char_index)
{
    gboolean retval = FALSE;
    outbuf->last_used_table = table; /* assume base table */
    while (*char_ptr)
    {
	if (!braille_table_encode_chunk_to_buffer (table, outbuf, char_ptr, char_index))
	    retval = TRUE;
    }
    return retval;
}

static BrailleContext *
tok_to_context (gchar *tok)
{
    if (strlen (tok) < 1) 
    {
	return BRAILLE_CONTEXT_ALL;
    }
    else 
    {
	BrailleContext **contexts = braille_get_contexts ();
	gint i = 0;
	while (contexts[i]) 
	{
	    if (!strcmp (tok, contexts[i]->name)) {
		return contexts[i];
	    }
	    ++i;
	}
    }
    return braille_context_add_new (g_strdup (tok));
}

static gulong
tok_to_int (gchar *tok)
{
    guint base = 10;
    gchar *cp = tok;
    
    if (*tok == 'U' && (strlen (tok) > 2))
    {
	base = 16;
	cp = tok+2;
    }
    else if (!strncmp (tok, "0x", 2) && (strlen (tok) > 2))
    {
	base = 16;
	cp = tok+2;
    }
    else if (*tok == '\\')
    {
	base = 8;
	cp = tok+1;
    }
    else
    {
	base = 10;
    }
    
    return g_ascii_strtoull (cp, NULL, base);
}

static UCSBlock*
ucs_block_new (gulong min, gulong max, BrailleContext *in_context, BrailleContext *out_context)
{
    UCSBlock *block = g_new0 (UCSBlock, 1);
    block->min = min;
    block->max = max;
    block->data = g_malloc0 (sizeof (gchar *) * (max - min + 1));
    block->context = in_context;
    block->out_context = out_context;
    g_assert (in_context);
    g_assert (out_context);
    g_assert (in_context->name);
    g_assert (out_context->name);
    return block;
}

static void
braille_table_add_block (BrailleTable *table, UCSBlock *block)
{
    g_return_if_fail (table);

    table->n_blocks++;

    if (table->blocks)
	table->blocks = g_renew (UCSBlock *, table->blocks, table->n_blocks);
    else
	table->blocks = g_new (UCSBlock *, table->n_blocks);

    table->blocks[table->n_blocks-1] = block;
}

static void
braille_table_add_string (BrailleTable *table, gchar *in_string, gchar *out_string, BrailleWordState match_type, BrailleContext *context)
{
    MatchStringKey *key;
    MatchStringValue *value;

    g_return_if_fail (table);

    if (!table->strings)
    {
	table->strings = g_hash_table_new (NULL, NULL);
    }

    key = g_new0 (MatchStringKey, 1);
    value = g_new0 (MatchStringValue, 1);

    key->string = g_strdup (in_string);
    key->match_type = match_type;
    key->context = context;

    value->string = g_strdup (out_string);
    value->match_len = g_utf8_strlen (in_string, -1);

    g_hash_table_insert (table->strings, key, value);
}


static void
braille_table_add_suffix (BrailleTable *table, gchar *suffix_string, gchar *suffix_name)
{
    gint n_suffixes = 0;
    BrSuffix *suffix = g_new0 (BrSuffix, 1);

    g_return_if_fail (table);

    if (table->suffixes)
    {
	while (table->suffixes[n_suffixes]) n_suffixes++;
	table->suffixes = g_renew (BrSuffix *, table->suffixes, n_suffixes + 1);
    }
    else {
	n_suffixes = 1;
	table->suffixes = g_new0 (BrSuffix *, n_suffixes + 1);
    }

    suffix->s = suffix_string;
    suffix->name = suffix_name;
    suffix->context = tok_to_context (suffix_name);
    table->suffixes[n_suffixes - 1] = suffix;
    table->suffixes[n_suffixes] = NULL;
}

gchar *
braille_table_check_suffix (BrailleTable *table, const gchar *string, BrailleContext **context)
{
    if (table->suffixes) 
    {
	int i = 0;
	while (table->suffixes[i] && strlen (table->suffixes[i]->s))
	{
	    if (g_str_has_prefix (string, table->suffixes[i]->s))
	    {
		*context = table->suffixes[i]->context;
		return g_strdup (""); /* TODO: suffix to 'prefix' string */
	    }
	    ++i;
	}
    }

    return NULL;
}


static void
string_list_add (gchar ***list, gchar *addstring)
{
    gint n_strings = 0;

    if (*list)
    {
	while ((*list)[n_strings]) n_strings++;
    }

    if (*list)
	*list = g_renew (gchar*, *list, n_strings + 2);
    else
	*list = g_new0 (gchar*, 2);

    (*list)[n_strings] = addstring;
    (*list)[n_strings+1] = NULL;
}

/*
 ************************************ 
 *
 * Braille Table utility functions 
 *
 ************************************
 */

/**
 * braille_table_construct_from_file:
 * @filename: the name of a file containing a braille encoding table.  If this is not
 * a fully-qualified pathname, the default braille table path will be used
 * (on gnome platforms, usually /usr/share/gnome-braille/)
 *
 * Return value: a #BrailleTable instance, or NULL if no table could be initialized from the 
 *               input file.
 **/
BrailleTable *braille_table_construct_from_file (gchar *filename)
{
    GIOChannel *io;

    if (filename)
    { 
	gchar *old_dirname;
	gchar *tbl_dirname;
	BrailleTable *table;

	if (g_file_test (filename, G_FILE_TEST_EXISTS))
	    filename = g_strdup (filename);
	else 
	{
	    gchar *tmp = filename;
	    filename = g_build_filename (PKGDATADIR, filename, NULL);
	    if (!g_file_test (filename, G_FILE_TEST_EXISTS))
	    {
		g_free (filename);
		filename = g_build_filename (GNOME_BRAILLE_DATADIR, tmp, NULL);
		if (!g_file_test (filename, G_FILE_TEST_EXISTS))
		{
		    g_free (filename);
		    filename = g_build_filename ("..", "tables", tmp, NULL);
		}
	    }
	}
	if (g_file_test (filename, G_FILE_TEST_EXISTS))
	    io = g_io_channel_new_file (filename, "r", NULL);
	else 
	    g_warning ("file %s not found", filename);

	if (!io)
	{
	    g_free (filename);
	}
	else
	{
	    tbl_dirname = g_path_get_dirname (filename);
	    g_free (filename);
	    old_dirname = g_get_current_dir ();
	    if (!strcmp (tbl_dirname, old_dirname) || chdir (tbl_dirname) == -1)
	    {
		g_free (tbl_dirname);
		tbl_dirname = NULL;
	    }
	    table = braille_table_construct_from_stream (io);
	    if (tbl_dirname)
	    {
		chdir (old_dirname);
		g_free (tbl_dirname);
	    }
	    g_free (old_dirname);
	    return table;
	}
    }
    return NULL;
}

/**
 * braille_table_construct_from_stream:
 * @io: a #GIOChannel whose contents can be parsed into a braille table.
 *
 * Return value: a #BrailleTable instance, or NULL if no table could be initialized from the 
 *               input stream.
 **/
BrailleTable *braille_table_construct_from_stream (GIOChannel *io)
{
    GError *error = NULL;
    GIOStatus status;
    GString *string = g_string_new (NULL);
    UCSBlock *block = NULL;
    BrailleTable *table = NULL;
    TableLineFormat line_format;
    gchar *encoding = NULL;
    gchar **tokens = NULL;
    gchar cbuf[7];
    gboolean in_block = FALSE;
    gint line;
    gint n_blocks = 0;

    braille_get_contexts (); /* initializes */
    status = g_io_channel_read_line_string (io, string, NULL, &error);
    if (status != G_IO_STATUS_NORMAL) g_warning ("I/O status=%d reading braille table", status);
    g_return_val_if_fail ((status == G_IO_STATUS_NORMAL || status == G_IO_STATUS_AGAIN), NULL);

    tokens = g_strsplit (string->str, " ", 2);
    if (tokens[0] && !strcmp (tokens[0], "ENCODING"))
    {
	encoding = tokens[1];
    }

    g_message ("channel encoding=%s; content encoding=%s", g_io_channel_get_encoding (io), tokens[0] ? tokens[1] : NULL);

    if (encoding && strcmp (g_io_channel_get_encoding (io), encoding))
    {
	g_io_channel_seek_position (io, 0, G_SEEK_SET, &error);
	g_io_channel_set_encoding (io, encoding, &error);
	g_io_channel_read_line_string (io, string, NULL, &error);
    }
    g_strfreev (tokens);

    table = g_new0 (BrailleTable, 1);
    /* TODO: explicit init func instead of relying on zeroing */

    line = 2;
    while ((status = g_io_channel_read_line_string (io, string, NULL, &error)) != G_IO_STATUS_EOF)
    {
	if (status == G_IO_STATUS_AGAIN) 
	{
	    g_message ("wait on reading Braille Table...");
	    continue;
	}
	else if (status == G_IO_STATUS_ERROR)
	{
	    g_io_channel_unref (io);
	    g_string_free (string, TRUE);
	    return NULL;
	}
	if (string->str && *string->str != '#' && *string->str != '\n') 
	{
	    g_strdelimit (string->str, "\n\t\r", ' ');
	    tokens = g_strsplit (string->str, " ", 20);

	    if (!strcmp (tokens[0], "NAME"))
	    {
		gint j = 1;
		gchar **nametokens = g_strsplit (string->str, "\"", 20);
		if (!nametokens[0]) 
		{
		    g_warning ("No names parsed in NAME field\n.");
		    continue;
		}
		while (nametokens[j])
		{
		    g_strstrip (nametokens[j]);
		    if (strlen (nametokens[j]) > 0)
		    {
			string_list_add (&table->names, g_strdup (nametokens[j]));
		    }
		    ++j;
		}
		g_strfreev (nametokens);
	    }
	    else if (!strcmp (tokens[0], "LOCALES"))
	    {
		gint j = 1;
		while (tokens[j])
		{
		    string_list_add (&table->locales, g_strdup (tokens[j]));
		    ++j;
		}
	    }
	    else if (!strcmp (tokens[0], "UCS-BLOCK"))
	    {
		if (!strcmp (tokens[1], "START"))
		{
		    BrailleContext *in_context = BRAILLE_CONTEXT_ALL, *out_context = BRAILLE_CONTEXT_ALL;
		    in_block = TRUE;
		    if (tokens[5]) 
		    {
			in_context = tok_to_context (tokens[5]);
			if (tokens[6]) 
			{
			    out_context = tok_to_context (tokens[6]);
			}
		    }
		    block = ucs_block_new (tok_to_int (tokens[2]), tok_to_int (tokens[3]), in_context, out_context);
		    braille_table_add_block (table, block);
		    if (!strcmp (tokens[4], "FORMAT-UNICODE"))
		    {
			line_format = TABLE_LINE_FORMAT_UNICODE;
		    }
		    else
		    {
			line_format = TABLE_LINE_FORMAT_UTF8;
		    }
		}
		else if (!strcmp (tokens[1], "END"))
		{
		    in_block = FALSE;
		}
	    }
	    else if (!strcmp (tokens[0], "UNICODE-CHAR"))
	    {
		/* create a one-character block */
		BrailleContext *in_context = BRAILLE_CONTEXT_ALL, *out_context = BRAILLE_CONTEXT_ALL;
		gulong ucs = tok_to_int (tokens[1]);
		if (tokens[3]) 
		{
		    in_context = tok_to_context (tokens[3]);
		    if (tokens[4]) 
		    {
			out_context = tok_to_context (tokens[4]);
		    }
		}
		block = ucs_block_new (ucs, ucs, in_context, out_context);
		cbuf[g_unichar_to_utf8 (tok_to_int (tokens[2]), cbuf)] = '\0';
		block->data[0] = g_strdup (cbuf);
		braille_table_add_block (table, block);
	    }
	    else if (!strcmp (tokens[0], "UCS-CHAR"))
	    {
		/* create a one-character block */
		BrailleContext *in_context = BRAILLE_CONTEXT_ALL, *out_context = BRAILLE_CONTEXT_ALL;
		gulong ucs = g_utf8_get_char (tokens[1]);
		if (tokens[3]) 
		{
		    in_context = tok_to_context (tokens[3]);
		    if (tokens[4]) 
		    {
			out_context = tok_to_context (tokens[4]);
		    }
		}
		block = ucs_block_new (ucs, ucs, in_context, out_context);
		block->data[0] = g_strdup (tokens[2]);
		braille_table_add_block (table, block);
	    }
	    else if (!strcmp (tokens[0], "UNKNOWN-CHAR"))
	    {
		if (tokens[1] && !strcmp (tokens[1], "PASS-THROUGH"))
		    table->pass_through = TRUE;
		else if (tokens[1])
		    table->out_of_range = g_strdup (tokens[1]);
	    }
	    else if (!strcmp (tokens[0], "UCS-SUFFIX"))
	    {
		if (tokens[1] && tokens[2])
		    braille_table_add_suffix (table, g_strdup (tokens[1]), g_strdup (tokens[2]));
	    }
	    else if (!strcmp (tokens[0], "UTF8-STRING"))
	    {
		BrailleContext *context = BRAILLE_CONTEXT_ALL;
		BrailleWordState string_subst_type = BRAILLE_STRING_MATCH_ALL;
		if (tokens[1] && tokens[2])
		{
		    if (tokens[3])
		    {
			string_subst_type = match_string_type_from_string (tokens[3]);
			if (tokens[4]) 
			    context = tok_to_context (tokens[4]);
		    }
		    braille_table_add_string (table, g_strdup (tokens[1]), g_strdup (tokens[2]), 
					      string_subst_type, context);
		}
	    }
	    else if (!strcmp (tokens[0], "DELEGATE"))
	    {
		if (tokens[1])
		{
		    if (!strcmp (tokens[1], "FILE")) 
			table->alternate = braille_table_construct_from_file (tokens[2]);
		    if (tokens[2] && tokens[3] && strlen (tokens[3]))
		    {
			    table->context_push_string = g_strdup (tokens[3]);
			    if (tokens[4] && strlen (tokens[4])) 
			    {
				    table->context_pop_string = g_strdup (tokens[4]);
				    if (tokens[5] && strlen (tokens[5]))
					    table->char_esc_string = g_strdup (tokens[5]);
			    }
		    }
		}
	    }	    
	    else /* default assumption: line is utf8/utf8 pair within a code block */
	    {
		gint ucs, n;
		gchar *cells;
		gchar **celltokens;
		
		if (!in_block) g_warning ("Error reading braille table, line %d", line);
		g_return_val_if_fail (in_block, NULL);

		switch (line_format)
		{
		    case TABLE_LINE_FORMAT_UNICODE:
			/* read a comma-delimited list of unicode points, and concatenate to utf-8 */
			ucs = tok_to_int (tokens[0]);
			celltokens = g_strsplit (tokens[1], ",", 10);
			n = 0;
			cells = g_strdup ("");
			while (celltokens[n])
			{
			    cbuf[g_unichar_to_utf8 (tok_to_int (tokens[2]), cbuf)] = '\0';
			    gchar *tmp = g_strconcat (cells, cbuf, NULL);
			    g_free (cells);
			    cells = tmp;
			}
			g_strfreev (celltokens);
			break;
		    case TABLE_LINE_FORMAT_UTF8:
		    default:
			ucs = g_utf8_get_char (tokens[0]);
			cells = g_strdup (tokens[1]);
			break;
		}

		if (ucs <= block->max) {
		    block->data[ucs - block->min] = cells;
		}
	    }
	    g_strfreev (tokens);
	}
	else {
	    g_message ("comment on line %d %s", line, string->str);
	}
	++line;
    }

    g_string_free (string, TRUE);

    return table;
}

BrailleOutputBuffer *
braille_output_buffer_new (void)
{
    BrailleOutputBuffer *buf = g_new (BrailleOutputBuffer, 1);
    buf->out_string = g_malloc0 (1024);
    buf->offsets = g_malloc0 (1024 * sizeof (guint));
    buf->size = 1024;
    buf->len = 0;
    buf->bytelen = 0;
    buf->offset_size = 1024;
    buf->context = BRAILLE_CONTEXT_ALPHA;
    return buf;
}

void
braille_output_buffer_free (BrailleOutputBuffer *buf)
{
    g_free (buf->out_string);
    g_free (buf->offsets);
    g_free (buf);
    return;
}

