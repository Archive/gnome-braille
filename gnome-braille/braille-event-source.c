/*
 * gnome-braille, a braille encoding and output system for GNOME, 
 *                and Unix-like operating systems.
 *
 * Copyright Sun Microsystems Inc. 2004
 *
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "braille-event-source.h"

GType
braille_event_source_get_type (void)
{
    static GType type = 0;
    if (!type)
    {
	static const GTypeInfo info =
	    {
		sizeof (BrailleEventSourceInterface),
		(GBaseInitFunc) NULL,
		(GBaseFinalizeFunc) NULL,
	    };
	type = g_type_register_static (G_TYPE_INTERFACE, "BrailleEventSourceInterface", &info, 0);
    }
    return type;
}

/**
 * braille_event_source_set_event_handler:
 * @event_source: the #BrailleEventSource instance of interest.
 * @handler: the #BrailleNotifyFunc to notify when an event occurs.
 *
 * Register an event handler for braille events.
 **/
void braille_event_source_set_event_handler (BrailleEventSource *event_source, 
					     BrailleNotifyFunc callback)
{
    BrailleEventSourceInterface *iface;
    g_return_if_fail (BRAILLE_IS_EVENT_SOURCE (event_source));

    iface = BRAILLE_EVENT_SOURCE_GET_INTERFACE (event_source);

    if (iface != NULL && iface->set_event_handler != NULL) 
	iface->set_event_handler (event_source, callback);
}
