/*
 * gnome-braille, a braille encoding and output system for GNOME, 
 *                and Unix-like operating systems.
 *
 * Copyright Sun Microsystems Inc. 2004
 *
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "braille-driver.h"

GType
braille_driver_get_type (void)
{
    static GType type = 0;
    if (!type)
    {
	static const GTypeInfo info =
	    {
		sizeof (BrailleDriverInterface),
		(GBaseInitFunc) NULL,
		(GBaseFinalizeFunc) NULL,
	    };
	type = g_type_register_static (G_TYPE_INTERFACE, "BrailleDriverInterface", &info, 0);
    }
    return type;
}

/**
 * braille_driver_display_string:
 * @driver: the #BrailleDriver object of interest.
 * @string: a UTF-8 string.
 * @display_num: a #gint indicating the target display are of the device.
 *
 * Output @string to braille-cell-area @display_num on the braille device.
 * 
 * @string is UTF-8 text, but is converted on output to
 * the character encoding appropriate to the device.
 * Unicode characters in @string which correspond to 
 * Braille code points (U+2800 to U+28FF) are converted to
 * braille-dot commands.
 **/
BrailleDriverStatus braille_driver_display_string (BrailleDriver *driver, 
						   gchar *string, 
						   gint display_num)
{
    BrailleDriverInterface *iface;
    g_return_val_if_fail (BRAILLE_IS_DRIVER (driver), BRAILLE_DRIVER_STATUS_INVALID);

    iface = BRAILLE_DRIVER_GET_INTERFACE (driver);

    if (iface != NULL && iface->display_string != NULL) 
	return iface->display_string (driver, string, display_num);
    else
	return BRAILLE_DRIVER_STATUS_NOIMPL;
}

/**
 * braille_driver_switchpos_to_offset:
 * @driver: the #BrailleDriver object of interest.
 * @pos: the braille switch position being queried.
 * @display_num: a #gint indicating the target display are of the device.
 *
 * Converts a braille driver switch position to an offset in the currently
 * displayed character string.  This conversion will take into account any
 * scrolling operations currently in effect on the string.
 *
 * Return value: the character offset in the most-recently-displayed string
 * corresponding to switch @pos, or "-1" on error.
 **/
gint braille_driver_switchpos_to_offset (BrailleDriver *driver, gint pos, gint display_num)
{
    BrailleDriverInterface *iface;
    g_return_val_if_fail (BRAILLE_IS_DRIVER (driver), -1);

    iface = BRAILLE_DRIVER_GET_INTERFACE (driver);

    if (iface != NULL && iface->switchpos_to_offset != NULL) 
	return iface->switchpos_to_offset (driver, pos, display_num);
    else
	return -1; /* means, error, don't know */
}

/**
 * braille_driver_set_unknown_char:
 * @driver: the #BrailleDriver object of interest.
 * @c: a #gunichar in the range U+2800 to U+28FF.
 *
 * Specify the dot pattern (as a unicode character between
 * U+2800 and U+28FF) or character displayed on the display
 * when braille_driver_display_string is passed a character 
 * outside the range displayable by @driver.
 * This is only used when passing strings to the device, as
 * opposed to dot-pattern commands.
 **/
void braille_driver_set_unknown_char (BrailleDriver *driver, gunichar c)
{
    BrailleDriverInterface *iface;
    g_return_if_fail (BRAILLE_IS_DRIVER (driver));

    iface = BRAILLE_DRIVER_GET_INTERFACE (driver);

    if (iface != NULL && iface->set_unknown_char != NULL) 
	iface->set_unknown_char (driver, c);
}

/**
 * braille_driver_get_num_displays:
 * @driver: the #BrailleDriver object of interest.
 *
 * Return value: a #gint indicating the number of display regions in the driver.
 **/
gint braille_driver_get_num_displays (BrailleDriver *driver)
{
    BrailleDriverInterface *iface;
    g_return_val_if_fail (BRAILLE_IS_DRIVER (driver), 0);

    iface = BRAILLE_DRIVER_GET_INTERFACE (driver);

    if (iface != NULL && iface->get_num_displays != NULL) 
	return iface->get_num_displays (driver);
    else
	return 1; /* note that if unimplemented in the driver, we assume only one display */
}

/**
 * braille_driver_get_display_name:
 * @driver: the #BrailleDriver object of interest.
 * @display_num: a #gint indicating the display region to query.
 *
 * Get a string identifying a display region of a #BrailleDriver.
 *
 * Return value: a UTF-8 string naming the display region, e.g. "status", 
 *      or NULL if no name is available.
 **/
gchar *braille_driver_get_display_name (BrailleDriver *driver, gint display_num)
{
    BrailleDriverInterface *iface;
    g_return_val_if_fail (BRAILLE_IS_DRIVER (driver), NULL);

    iface = BRAILLE_DRIVER_GET_INTERFACE (driver);

    if (iface != NULL && iface->get_display_name != NULL) 
	return iface->get_display_name (driver, display_num);
    else
	return NULL;
}

/**
 * braille_driver_get_display_cellcount:
 * @driver: the #BrailleDriver object of interest.
 * @display_num: a #gint indicating the display region to query.
 *
 * Get the number of cells in a given display region.
 *
 * Return value: a #gint giving the number of available cells in @display_num, or -1 on error.
 **/
gint braille_driver_get_display_cellcount (BrailleDriver *driver, gint display_num)
{
    BrailleDriverInterface *iface;
    g_return_val_if_fail (BRAILLE_IS_DRIVER (driver), -1);

    iface = BRAILLE_DRIVER_GET_INTERFACE (driver);

    if (iface != NULL && iface->get_display_cellcount != NULL) 
	return iface->get_display_cellcount (driver, display_num);
    else
	return -1;
}


/**
 * braille_driver_scroll_display:
 * @driver: the #BrailleDriver object of interest.
 * @display_num: a #gint indicating the display region to scroll.
 * @units_to_scroll: a #gint indicating the direction and magnitude of the
 *          scrolling operation.
 * @action: a #BrailleDriverScrollAction indicating the scrolling units, 
 *          e.g. cells, pages, characters.
 *
 * Scrolls a display region forwards or backwards.  If @action is
 * #BRAILLE_SCROLL_CELLS or #BRAILLE_SCROLL_CHARS, @units_to_scroll
 * indicates the number of cells or characters to scroll.  If @action
 * is #BRAILLE_SCROLL_PAGES, @units_to_scroll should be 1 or -1.  Negative
 * values indicate "backwards" scrolling.
 *
 * Return value: a #BrailleDriverStatus indicating whether the operation was
 *               successfully carried out.
 **/
BrailleDriverStatus braille_driver_scroll_display (BrailleDriver *driver, 
						   gint display_num, 
						   gint units_to_scroll, 
						   BrailleDriverScrollAction action)
{
    BrailleDriverInterface *iface;
    g_return_val_if_fail (BRAILLE_IS_DRIVER (driver), BRAILLE_DRIVER_STATUS_INVALID);

    iface = BRAILLE_DRIVER_GET_INTERFACE (driver);

    if (iface != NULL && iface->scroll_display != NULL) 
	return iface->scroll_display (driver, display_num, units_to_scroll, action);
    else
	return BRAILLE_DRIVER_STATUS_NOIMPL;
}

/**
 * braille_driver_set_virtual_caret:
 * @driver: the #BrailleDriver object of interest.
 * @caret_offset: a #gint indicating the cell offset where the caret should be placed.
 * @display_num: a #gint indicating the display region to query.
 *
 * Place the display's virtual caret at cell @caret_offset.
 *
 * Return value: a #BrailleDriverStatus indicating whether the caret was 
 *               positioned as requested.
 **/
BrailleDriverStatus braille_driver_set_virtual_caret (BrailleDriver *driver,
						      gint caret_offset,
						      gint display)
{
    BrailleDriverInterface *iface;
    g_return_val_if_fail (BRAILLE_IS_DRIVER (driver), BRAILLE_DRIVER_STATUS_INVALID);

    iface = BRAILLE_DRIVER_GET_INTERFACE (driver);

    if (iface != NULL && iface->set_virtual_caret != NULL) 
	return iface->set_virtual_caret (driver, caret_offset, display);
    else
	return BRAILLE_DRIVER_STATUS_NOIMPL;
}

/**
 * braille_driver_set_scrolling_switch:
 * @driver: the #BrailleDriver object of interest.
 * @display_num: a #gint indicating the display region to query.
 * @switch: a #gint corresponding to a switch number as reported by @driver.
 * @units_to_scroll: a #gint indicating the magnitude and direction of the action.
 * @action: a #BrailleDriverScrollAction type.
 * @notify: whether or not to continue notifying listening clients of this event.
 *
 * Associate a particular switch on the braille device with a scrolling action, 
 * such that subsequent presses of this switch result in @display_num scrolling
 * @units_to_scroll units of the type indicated by @action.  For instance,
 * a call to
 *  status = braille_driver_set_scrolling_switch (driver, 0, 2, -1, BRAILLE_SCROLL_CELLS);
 * will mean, if status == BRAILLE_STATUS_NORMAL, that each subsequent press of switch 2
 * will cause display 0 to scroll one cell backwards.
 * If @notify is FALSE, events originating from the specified switch will no longer be sent
 * to listening clients until scrolling-switch association is reset by another call to
 * braille_driver_set_scrolling_switch() with the same display_num and action values.
 *
 * Return value: a #BrailleDriverStatus indicating whether the association was
 *               successfully carried made.
 **/
BrailleDriverStatus braille_driver_set_scrolling_switch (BrailleDriver *driver, 
							 gint display_num, 
							 gint switchnum, 
							 gint units_to_scroll, 
							 BrailleDriverScrollAction action,
							 gboolean notify)
{
    BrailleDriverInterface *iface;
    g_return_val_if_fail (BRAILLE_IS_DRIVER (driver), BRAILLE_DRIVER_STATUS_INVALID);

    iface = BRAILLE_DRIVER_GET_INTERFACE (driver);

    if (iface != NULL && iface->set_scrolling_switch != NULL) 
	return iface->set_scrolling_switch (driver, display_num, switchnum, units_to_scroll, action, notify);
    else
	return BRAILLE_DRIVER_STATUS_NOIMPL;
}

