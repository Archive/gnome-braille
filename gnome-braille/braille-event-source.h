/*
 * gnome-braille, a braille encoding and output system for GNOME, 
 *                and Unix-like operating systems.
 *
 * Copyright Sun Microsystems Inc. 2004
 *
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __BRAILLE_EVENT_SOURCE_H__
#define __BRAILLE_EVENT_SOURCE_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <glib-object.h>
#include "braille-driver.h"
#include "braille-encoder.h"

#define BRAILLE_TYPE_EVENT_SOURCE               (braille_event_source_get_type ())
#define BRAILLE_IS_EVENT_SOURCE(obj)            G_TYPE_CHECK_INSTANCE_TYPE ((obj), BRAILLE_TYPE_EVENT_SOURCE)
#define BRAILLE_EVENT_SOURCE(obj)               G_TYPE_CHECK_INSTANCE_CAST ((obj), BRAILLE_TYPE_EVENT_SOURCE, BrailleEventSource)
#define BRAILLE_EVENT_SOURCE_GET_INTERFACE(obj) G_TYPE_INSTANCE_GET_INTERFACE ((obj), BRAILLE_TYPE_EVENT_SOURCE, BrailleEventSourceInterface)

typedef struct _BrailleEventSource BrailleEventSource;
typedef struct _BrailleEventSourceInterface BrailleEventSourceInterface;
/**
 * BrailleKeyType:
 *
 * @BRAILLE_KEY_TYPE_DOTS: Keycode represents dot pattern, where
 * braille dots from LSB to MSB are [12345678]
 * @BRAILLE_KEY_TYPE_UCS4: Keycode represents a character in UCS4 encoding.
 * @BRAILLE_KEY_TYPE_CONTROL: Keycode represents a 'switch number' for
 * arbitrary control functions, i.e. number has no semantic meaning.
 * @BRAILLE_KEY_TYPE_CELL: Key is associated with a particular physical
 * braille cell on the display, and results from user activation of
 * that key or switch.
 * @BRAILLE_KEY_TYPE_PROXIMITY: Key is associated with a physical braille
 * cell, and indicates that the user's finger has come into proximity of
 * the cell.
 *
 **/
typedef enum
{
    BRAILLE_KEY_TYPE_DOTS,
    BRAILLE_KEY_TYPE_UCS4,
    BRAILLE_KEY_TYPE_CONTROL,
    BRAILLE_KEY_TYPE_CELL,
    BRAILLE_KEY_TYPE_PROXIMITY,
    BRAILLE_KEY_TYPE_LAST_DEFINED
} BrailleKeyType;

typedef enum
{
    BRAILLE_EVENT_KEY,
    BRAILLE_EXCEPTION_DRIVER,
    BRAILLE_EXCEPTION_ENCODER,
    BRAILLE_EXCEPTION_LAST_DEFINED
} BrailleEventCategory;

typedef struct _BrailleEvent
{
    BrailleEventCategory type;
    struct
    {
	BrailleKeyType keytype;
	guint32 keycode;
    } key;
    union 
    {
	BrailleDriverStatus status;
	BrailleEncoderError encoder_err;
    } err;
} BrailleEvent;

typedef void (*BrailleNotifyFunc) (BrailleEvent event);

struct _BrailleEventSourceInterface
{
    GTypeInterface parent;

    void (*set_event_handler)     (BrailleEventSource *event_source,
				   BrailleNotifyFunc  notify_func);
};



/**
 * braille_event_source_set_event_handler:
 * @event_source: the #BrailleEventSource instance of interest.
 * @handler: the #BrailleNotifyFunc to notify when an event occurs.
 *
 * Register an event handler for braille events.
 **/
void braille_event_source_set_event_handler (BrailleEventSource *event_source, 
					     BrailleNotifyFunc handler);
#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __BRAILLE_EVENT_SOURCE_H__ */
