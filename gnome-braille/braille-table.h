/**
 * gnome-braille, a braille encoding and output system for GNOME, 
 *                and Unix-like operating environments.
 *
 * Copyright Sun Microsystems Inc. 2004, 2005
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 **/

#ifndef __BRAILLE_TABLE_H__
#define __BRAILLE_TABLE_H__


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <glib-object.h>
#include "braille-encoder.h" /* for BrailleContext */

typedef struct _UCSBlock {
  BrailleContext *context;
  BrailleContext *out_context;
  gunichar min;
  gunichar max;
  /* array of null-terminated UTF-8 strings corresponding to dots */
  gchar **data; 
} UCSBlock;

typedef struct _BrSuffix {
  gchar *s;
  BrailleContext *context;
  gchar *name;
} BrSuffix;

typedef struct _BrailleTable BrailleTable;

struct _BrailleTable {
  gchar       **names;
  gchar       **locales;
  UCSBlock    **blocks;
  gint          n_blocks;
  BrSuffix    **suffixes;  
  GHashTable   *strings;
  BrailleTable *alternate; /* null terminated cascade */
  gchar        *out_of_range; /* UTF-8 string */
  gchar        *context_push_string; /* UTF-8 chars indicating context change (in) */
  gchar        *context_pop_string; /* UTF-8 chars indicating context change (return) */
  gchar        *char_esc_string; /* prefix 'escapes' (marks as 'foreign') one char */
  gboolean      pass_through; /* whether to "pass through" unconverted chars */
};


typedef enum {
    BRAILLE_STRING_MATCH_ALL,
    BRAILLE_STRING_MATCH_PREFIX,
    BRAILLE_STRING_MATCH_SUFFIX,
    BRAILLE_STRING_MATCH_EXACT
} BrailleWordState;

typedef struct {
    gchar *out_string;
    guint *offsets;
    guint size;
    guint len;
    guint bytelen;
    guint offset_size;
    BrailleContext *context;
    BrailleWordState match_state;
    BrailleTable   *last_used_table;
    BrailleEncoderFlags flags;
} BrailleOutputBuffer;

/* Braille Table utility functions */

/**
 * braille_table_construct_from_file:
 * @filename: the fully-qualified pathname of a file containing a braille encoding table.
 *
 * Return value: a #BrailleTable instance, or NULL if no table could be initialized from the 
 *               input file.
 **/
BrailleTable *braille_table_construct_from_file (gchar *filename);

/**
 * braille_table_construct_from_stream:
 * @io: a #GIOChannel whose contents can be parsed into a braille table.
 *
 * Return value: a #BrailleTable instance, or NULL if no table could be initialized from the 
 *               input stream.
 **/
BrailleTable *braille_table_construct_from_stream (GIOChannel *io);

/**
 * braille_table_encode_to_buffer:
 **/
gboolean braille_table_encode_to_buffer (BrailleTable *table, BrailleOutputBuffer *outbuf, const gchar **char_ptr, gint *char_index);

/**
 * braille_table_check_suffix:
 **/
gchar *braille_table_check_suffix (BrailleTable *table, const gchar *string, BrailleContext **context);

/**
 * braille_table_:
 **/
BrailleOutputBuffer *braille_output_buffer_new (void);

/**
 * braille_table_:
 **/
void braille_output_buffer_free (BrailleOutputBuffer *buf);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __BRAILLE_TABLE_H__ */
