/*
 * gnome-braille, a braille encoding and output system for GNOME, 
 *                and Unix-like operating systems.
 *
 * Copyright Sun Microsystems Inc. 2004
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "braille-encoder.h"

static BrailleContext** _braille_contexts = NULL;

BrailleContext* BRAILLE_CONTEXT_ALL;
BrailleContext* BRAILLE_CONTEXT_ALPHA;

BrailleContext*
braille_context_add_new (gchar *name)
{
    BrailleContext *context;
    gint n_contexts = 0;
    if (_braille_contexts == NULL)
    {
	context = g_new (BrailleContext, 1);
	context->name = name;
	n_contexts = 1;
	_braille_contexts = g_new (BrailleContext *, n_contexts + 1);
    }
    else
    {
	while (_braille_contexts[n_contexts]) 
	{
	    if (!strcmp (_braille_contexts[n_contexts]->name, name))
	    {
		return _braille_contexts[n_contexts];
	    }
	    n_contexts++;
	}
	n_contexts++;
	_braille_contexts = g_renew (BrailleContext *, _braille_contexts, n_contexts + 1);
    }
    context = g_new (BrailleContext, 1);
    context->name = name;
    _braille_contexts[n_contexts - 1] = context;
    _braille_contexts[n_contexts] = NULL;

    return context;
}

BrailleContext**
braille_get_contexts (void)
{
    if (_braille_contexts == NULL)
    {
	BRAILLE_CONTEXT_ALL = braille_context_add_new (g_strdup ("all"));
	BRAILLE_CONTEXT_ALPHA = braille_context_add_new (g_strdup ("alpha"));
    }
    return _braille_contexts;
}

gboolean
braille_context_match (BrailleContext *a, BrailleContext *b)
{
    return ((!a && !b) || (a == b) || (a && b && ((a->name == b->name) || !strcmp (a->name, b->name))));
}

GType
braille_encoder_get_type (void)
{
    static GType type = 0;
    if (!type)
    {
	static const GTypeInfo info =
	    {
		sizeof (BrailleEncoderInterface),
		(GBaseInitFunc) NULL,
		(GBaseFinalizeFunc) NULL,
	    };
	type = g_type_register_static (G_TYPE_INTERFACE, "BrailleEncoderInterface", &info, 0);
    }
    return type;
}

/** 
 * braille_encoder_translate_string:
 * @encoder: the #BrailleEncoder instance.
 * @string: a UTF-8 input string to translate.
 * @offsets: a pointer to a #guint pointer, into which the
 * corresponding offsets array will be placed.
 *
 * Convert a UTF-8 string to a string appropriate to a braille
 * device: the output string consists of either characters appropriate
 * to a braille device's input encoding, unichar braille codepoints
 * representing the encoded dot patterns, or a mixture of the two.
 * If @offsets is non-NULL, the array is filled with offsets into 
 * the input string which correspond to each UTF-8 character position 
 * in the output string, i.e. the value of offsets[2] is the character 
 * offset in @string corresponding to the third UTF-8 character in the 
 * output string.
 *
 * Return value: the translated UTF-8 string.
 **/
gchar* 
braille_encoder_translate_string (BrailleEncoder *encoder,
				  const gchar *string,
				  guint **offsets)
{
    BrailleEncoderInterface *iface;
    g_return_val_if_fail (BRAILLE_IS_ENCODER (encoder), NULL);

    iface = BRAILLE_ENCODER_GET_INTERFACE (encoder);

    if (iface != NULL && iface->translate_string != NULL) 
	return iface->translate_string (encoder, string, offsets);
    else
	return g_strdup (string); /* pass-through */
}

/** 
 * braille_encoder_set_alternate:
 * @encoder: the #BrailleEncoder instance.
 * @alternate: the #BrailleEncoder instance to which the first instance 
 *             should delegate translation/encoding of characters which it 
 *             cannot handle directly.
 *
 * Set another BrailleEncoder interface implementation to which
 *      this encoder will delegate the encoding characters which
 *      lie outside of its defined range, i.e. characters which 
 *      it does not recognize.  If there is no alternate,
 *      the encoder will pass these characters through unaltered.
 **/
void 
braille_encoder_set_alternate (BrailleEncoder *encoder,
			       BrailleEncoder *alternate)
{
    BrailleEncoderInterface *iface;
    g_return_if_fail (BRAILLE_IS_ENCODER (encoder));

    iface = BRAILLE_ENCODER_GET_INTERFACE (encoder);

    if (iface != NULL && iface->set_alternate != NULL) 
	iface->set_alternate (encoder, alternate);
}

/** 
 * braille_encoder_get_alternate:
 * @encoder: the #BrailleEncoder instance.
 *
 * Get the #BrailleEncoder interface implementation to which
 *      this encoder will delegate the encoding characters which
 *      lie outside of its defined range, i.e. characters which 
 *      it does not recognize.  If there is no alternate,
 *      returns NULL.
 **/
BrailleEncoder *braille_encoder_get_alternate (BrailleEncoder *encoder)
{
    BrailleEncoderInterface *iface;
    g_return_val_if_fail (BRAILLE_IS_ENCODER (encoder), NULL);

    iface = BRAILLE_ENCODER_GET_INTERFACE (encoder);

    if (iface != NULL && iface->get_alternate != NULL) 
	return iface->get_alternate (encoder);
}


/**
 * braille_encoder_chain_offsets:
 * @offsets_in:
 * @len_in:
 * @offsets_next:
 * @len_next:
 * 
 * Return value: a new set of offsets which adjusts @offsets_in by @offsets_next.
 *
 **/
guint*
braille_encoder_chain_offsets (guint *offsets_in, guint len_in, guint *offsets_next, guint len_next)
{
    /* TODO: Implement! */
    

    return offsets_next;
}
