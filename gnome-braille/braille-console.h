/*
 * gnome-braille, a braille encoding and output system for GNOME, 
 *                and Unix-like operating systems.
 *
 * Copyright Sun Microsystems Inc. 2005
 *
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __BRAILLE_CONSOLE_H__
#define __BRAILLE_CONSOLE_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <glib-object.h>

#define BRAILLE_TYPE_CONSOLE               (braille_console_get_type ())
#define BRAILLE_IS_CONSOLE(obj)            G_TYPE_CHECK_INSTANCE_TYPE ((obj), BRAILLE_TYPE_CONSOLE)
#define BRAILLE_CONSOLE(obj)               G_TYPE_CHECK_INSTANCE_CAST ((obj), BRAILLE_TYPE_CONSOLE, BrailleConsole)
#define BRAILLE_CONSOLE_GET_INTERFACE(obj) G_TYPE_INSTANCE_GET_INTERFACE ((obj), BRAILLE_TYPE_CONSOLE, BrailleConsoleInterface)

typedef struct _BrailleConsole BrailleConsole;
typedef struct _BrailleConsoleInterface BrailleConsoleInterface;

struct _BrailleConsoleInterface
{
    GTypeInterface parent;

    gint                (*display_string)   (BrailleConsole *console,
					     gchar *string, 
					     gint display_num,
					     guint **offsets);

    void                (*add_encoder)      (BrailleConsole *console,
					     BrailleEncoder *encoder);

    void                (*remove_encoder)   (BrailleConsole *console,
					     BrailleEncoder *encoder);

    BrailleEncoder*     (*get_encoder_list) (BrailleConsole *console,
					     BrailleEncoder *encoder);

    void                (*set_driver)       (BrailleConsole *console,
					     BrailleDriver);

    BrailleDriver*      (*get_driver)       (BrailleConsole *console);    

    void                (*key_notify_func)  (BrailleConsole *console,
					     BrailleKeyNotifyFunc *notify_func);

    void                (*err_notify_func)  (BrailleConsole *console,
					     BrailleErrNotifyFunc *notify_func);
    /* future expansion; compat */
    void (* reserved1) (void);
    void (* reserved2) (void);
    void (* reserved3) (void);
    void (* reserved4) (void);
};

/**
 * braille_console_outstring:
 * @driver: the #BrailleConsole object of interest.
 * @string: a UTF-8 string.
 *
 * Output @string to the default braille-cell-area on the braille console.
 *
 * @string is UTF-8 text, but is converted on output to
 * the character encoding appropriate to the device.
 * Unicode characters in @string which correspond to 
 * Braille code points (U+2800 to U+28FF) are converted to
 * braille-dot commands.
 *
 * returns: the number of characters (braille dot patterns or alphanumeric chars)
 * sent to the braille device, or -1 on error.
 **/
gint braille_console_outstring (BrailleConsole *console,
				gchar *string, 
				guint **offsets);

/**
 * braille_console_display_string:
 * @driver: the #BrailleConsole object of interest.
 * @string: a UTF-8 string.
 * @display_num: a #gint indicating the target display are of the device.
 *
 * Output @string to braille-cell-area @display_num on the braille console.
 *
 * @string is UTF-8 text, but is converted on output to
 * the character encoding appropriate to the device.
 * Unicode characters in @string which correspond to 
 * Braille code points (U+2800 to U+28FF) are converted to
 * braille-dot commands.
 *
 * returns: the number of characters (braille dot patterns or 
 * alphanumeric chars) sent to the braille device, or -1 on error.
 **/
gint braille_console_display_string (BrailleConsole *console,
				     gchar *string, 
				     gint display_num,
				     guint **offsets);

/**
 * braille_console_add_encoder:
 * @driver: the #BrailleConsole object of interest.
 **/
void braille_console_add_encoder (BrailleConsole *console,
				  BrailleEncoder *encoder);

/**
 * braille_console_remove_encoder:
 * @driver: the #BrailleConsole object of interest.
 **/
void braille_console_remove_encoder (BrailleConsole *console,
				     BrailleEncoder *encoder);

/**
 * braille_console_set_encoder_list:
 * @driver: the #BrailleConsole object of interest.
 **/
void braille_console_set_encoder_list (BrailleConsole *console,
				       va_list var_args);

/**
 * braille_console_get_encoder_list:
 * @driver: the #BrailleConsole object of interest.
 **/
BrailleEncoder* braille_console_get_encoder_list (BrailleConsole *console,
						  BrailleEncoder *encoder);

/**
 * braille_console_set_driver:
 * @driver: the #BrailleConsole object of interest.
 **/
void braille_console_set_driver (BrailleConsole *console,
				 BrailleDriver);

/**
 * braille_console_get_driver:
 * @driver: the #BrailleConsole object of interest.
 **/
BrailleDriver* braille_console_get_driver (BrailleConsole *console);    

/**
 * braille_console_set_key_notify_func:
 * @driver: the #BrailleConsole object of interest.
 **/
void braille_console_set_key_notify_func (BrailleConsole *console,
					  BrailleKeyNotifyFunc *notify_func);

/**
 * braille_console_set_err_notify_func:
 * @driver: the #BrailleConsole object of interest.
 **/
void braille_console_set_err_notify_func (BrailleConsole *console,
					  BrailleErrNotifyFunc *notify_func);

/**
 * braille_console_new:
 * @driver: The #BrailleDriver instance to which this #BrailleConsole
 * will send its output.
 * @encoder: The #BrailleEncoder instance to associate with this driver,
 * or NULL to defer the association of an encoder (or list of encoders).
 *
 * Creates a new #BrailleConsole instance using the specified
 * #BrailleDriver and #BrailleEncoder.
 * If either @driver or @encoder is NULL, the desired #BrailleDriver
 * and/or #BrailleEncoder must be specified via calls to
 * #braille_console_set_driver and/or #braille_console_set_encoder_list.
 * Clients should call #braille_console_release when they no longer
 * need to access a #BrailleConsole instance.
 * Note that a #BrailleConsole takes ownership of the #BrailleDriver
 * and #BrailleEncoder instances specified in this call, so that 
 * the #BrailleConsole client need not, and should not, call
 * #braille_driver_release or #braille_encoder_release on @driver 
 * or @encoder in this circumstance.
 *
 * returns: a newly-created #BrailleConsole instance.
 **/
BrailleConsole* braille_console_new (BrailleDriver *driver, 
				     BrailleEncoder *encoder);

/**
 * braille_console_release:
 * @console: the #BrailleConsole which is being released by its client.
 *
 * Release a #BrailleEncoder from use.  Any associated resources
 * will be freed as a result, including any associated #BrailleDriver
 * and #BrailleEncoder instances.  
 * Clients should not perform any operations on
 * a #BrailleEncoder after they have released it.
 **/
void braille_console_release (BrailleConsole *console);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __BRAILLE_CONSOLE_H__ */
