/**
 * gnome-braille, a braille encoding and output system for GNOME, 
 *                and Unix-like operating environments.
 *
 * Copyright Sun Microsystems Inc. 2004, 2005
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 **/

#ifndef __BRAILLE_TABLE_ENCODER_H__
#define __BRAILLE_TABLE_ENCODER_H__


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <glib-object.h>
#include "braille-encoder.h"
#include "braille-table.h"

#define BRAILLE_TYPE_TABLE_ENCODER               (braille_table_encoder_get_type ())
#define BRAILLE_IS_TABLE_ENCODER(obj)            G_TYPE_CHECK_INSTANCE_TYPE ((obj), BRAILLE_TYPE_TABLE_ENCODER)
#define BRAILLE_TABLE_ENCODER(obj)               G_TYPE_CHECK_INSTANCE_CAST ((obj), BRAILLE_TYPE_TABLE_ENCODER, BrailleTableEncoder)
#define BRAILLE_TABLE_ENCODER_GET_CLASS(obj)     G_TYPE_INSTANCE_GET_CLASS ((obj), BRAILLE_TYPE_TABLE_ENCODER, BrailleTableEncoderClass)

typedef struct _BrailleTableEncoder BrailleTableEncoder;
typedef struct _BrailleTableEncoderClass BrailleTableEncoderClass;

/* 
 * base class which implements BrailleEncoder interface using 
 * Braille tables.  Unlike BrailleEncoder itself (which is an interface), 
 * BrailleTableEncoder is an actual GObject Class derived from GObject.
 */
struct _BrailleTableEncoder
{
    GObject parent;

    /* private data, don't poke it */
    BrailleTable   *table;
    BrailleEncoder *alternate;
};

struct _BrailleTableEncoderClass
{
    GObjectClass parent;

    void     (* set_table)            (BrailleTableEncoder *encoder, BrailleTable *table);

    /* future expansion; compat */
    void (* reserved1) (void);
    void (* reserved2) (void);
    void (* reserved3) (void);
    void (* reserved4) (void);
};

/**
 * braille_table_encoder_new:
 * @table: the #BrailleTable which should serve as the base for this encoder.
 *
 * Creates a #BrailleTable instance which uses @table for its 
 * dot or character conversions.  The @braille_table_encoder
 * instance should be freed with #braille_table_encoder_release () 
 * when it is no longer needed.
 *
 * returns: a newly-created #BrailleTableEncoder instance.
 **/
BrailleTableEncoder* braille_table_encoder_new (BrailleTable *table);

/**
 * braille_table_encoder_release:
 * @encoder: the #BrailleTableEncoder which is being released by its client.
 *
 * Release a #BrailleTableEncoder from use.  Any associated resources
 * may be freed as a result.  Clients should not perform any operations on
 * a #BrailleTableEncoder after they have released it.
 **/
void braille_table_encoder_release (BrailleTableEncoder *encoder);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __BRAILLE_TABLE_ENCODER_H__ */
