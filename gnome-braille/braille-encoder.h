/*
 * gnome-braille, a braille encoding and output system for GNOME, 
 *                and Unix-like operating systems.
 *
 * Copyright Sun Microsystems Inc. 2004
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __BRAILLE_ENCODER_H__
#define __BRAILLE_ENCODER_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <glib-object.h>

#define BRAILLE_TYPE_ENCODER               (braille_encoder_get_type ())
#define BRAILLE_IS_ENCODER(obj)            G_TYPE_CHECK_INSTANCE_TYPE ((obj), BRAILLE_TYPE_ENCODER)
#define BRAILLE_ENCODER(obj)               G_TYPE_CHECK_INSTANCE_CAST ((obj), BRAILLE_TYPE_ENCODER, BrailleEncoder)
#define BRAILLE_ENCODER_GET_INTERFACE(obj) G_TYPE_INSTANCE_GET_INTERFACE ((obj), BRAILLE_TYPE_ENCODER, BrailleEncoderInterface)

typedef enum
{
    BRAILLE_ENCODER_ERROR_UNKNOWN,
    BRAILLE_ENCODER_ERROR_LAST_DEFINED
} BrailleEncoderError;

/**
 * BrailleContext is used in situations where the braille
 * dot pattern appropriate to a UCS-4/unicode character is 
 * context-dependent, but still expressible as a per-character
 * lookup (for instance, Korean Braille).  It is also used
 * for things like number prefixes (which apply to a number but
 * not each digit), and embedded foreign language text or words
 * within a document.
 **/
typedef struct _BrailleContext {
    gchar *name;
} BrailleContext;

/* XXX Are bitfields a problem for language bindings? */
typedef guint32 BrailleEncoderFlags;

typedef enum
{
    BRAILLE_ENCODER_FLAGS_NO_CONTEXT_TAGS = (1 << 0)
} BrailleEncoderFlag;

typedef struct _BrailleEncoder BrailleEncoder;
typedef struct _BrailleEncoderInterface BrailleEncoderInterface;

struct _BrailleEncoderInterface
{
    GTypeInterface parent;
    
    gchar*    (*translate_string) (BrailleEncoder *encoder, const gchar *string, guint **offsets);
    void            (*set_alternate)    (BrailleEncoder *encoder, BrailleEncoder *alternate);
    BrailleEncoder* (*get_alternate)    (BrailleEncoder *encoder);
    
    /* future expansion; compat */
    void (* reserved1) (void);
    void (* reserved2) (void);
    void (* reserved3) (void);
    void (* reserved4) (void);
};

GType braille_encoder_get_type (void);

/**
 * braille_encoder_release:
 * @encoder: the #BrailleEncoder which is being released by its client.
 *
 * Release a #BrailleEncoder from use.  Any associated resources
 * may be freed as a result.  Clients should not perform any operations on
 * a #BrailleEncoder after they have released it.
 **/
void braille_encoder_release (BrailleEncoder *encoder);

/** 
 * braille_encoder_translate_string:
 * @encoder: the #BrailleEncoder instance.
 * @string: a UTF-8 input string to translate.
 * @offsets: a pointer to a #guint pointer, into which the
 * corresponding offsets array will be placed.
 *
 * Convert a UTF-8 string to a string appropriate to a braille
 * device: the output string consists of either characters appropriate
 * to a braille device's input encoding, unichar braille codepoints
 * representing the encoded dot patterns, or a mixture of the two.
 * If @offsets is non-NULL, the array is filled with offsets into 
 * the input string which correspond to each UTF-8 character position 
 * in the output string, i.e. the value of offsets[2] is the character 
 * offset in @string corresponding to the third UTF-8 character in the 
 * output string.
 *
 * Return value: the translated UTF-8 string, which should be 
 * freed by the caller after use.
 **/
gchar* braille_encoder_translate_string (BrailleEncoder *encoder,
					       const gchar *string,
					       guint **offsets);


/** 
 * braille_encoder_set_alternate:
 * @encoder: the #BrailleEncoder instance.
 * @alternate: the #BrailleEncoder instance to which the first instance 
 *             should delegate translation/encoding of characters which it 
 *             cannot handle directly.
 *
 * Set another BrailleEncoder interface implementation to which
 *      this encoder will delegate the encoding characters which
 *      lie outside of its defined range, i.e. characters which 
 *      it does not recognize.  If there is no alternate,
 *      the encoder will pass these characters through unaltered.
 **/
void braille_encoder_set_alternate (BrailleEncoder *encoder,
				    BrailleEncoder *alternate);


/** 
 * braille_encoder_get_alternate:
 * @encoder: the #BrailleEncoder instance.
 *
 * Get the #BrailleEncoder interface implementation to which
 *      this encoder will delegate the encoding characters which
 *      lie outside of its defined range, i.e. characters which 
 *      it does not recognize.  If there is no alternate,
 *      returns NULL.
 **/
BrailleEncoder *braille_encoder_get_alternate (BrailleEncoder *encoder);


/**
 * braille_encoder_chain_offsets:
 * @offsets_in:
 * @len_in:
 * @offsets_next:
 * @len_next:
 * 
 * Return value: a new set of offsets which adjusts @offsets_in by @offsets_next.
 *
 **/
guint* braille_encoder_chain_offsets (guint *offsets_in, guint len_in, guint *offsets_next, guint len_next);

BrailleContext* braille_context_add_new (gchar *name);

BrailleContext** braille_get_contexts (void);

gboolean braille_context_match (BrailleContext *a, BrailleContext *b);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __BRAILLE_ENCODER_H__ */
